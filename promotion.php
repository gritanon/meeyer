
	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/elements/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>ตรอ. ช่างใหญ่เซอร์วิส</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="index/css/linearicons.css">
			<link rel="stylesheet" href="index/css/owl.carousel.css">
			<link rel="stylesheet" href="index/css/font-awesome.min.css">
			<link rel="stylesheet" href="index/css/nice-select.css">			
			<link rel="stylesheet" href="index/css/magnific-popup.css">
			<link rel="stylesheet" href="index/css/bootstrap.css">
			<link rel="stylesheet" href="index/css/main.css">
			<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
		<link href="https://fonts.googleapis.com/css?family=Mitr" rel="stylesheet">

		</head>
		<style>
			html,body{
				font-family: 'Mitr', sans-serif;
				height: 100%;
			}
			size-b{
				color: #005c9e;
				font-size: 2.1em;
				font-weight: bolder;
			}
		</style>
		<body>
		<header id="header" id="home">
			    <div class="container">
			    	<div class="row align-items-center justify-content-between d-flex">
				      <div id="logo">
				        <a href="index.html"><img src="index/img/logoChangyai.png" alt="" title="" /></a>
				      </div>
				      <nav id="nav-menu-container">
				        <ul class="nav-menu">
						  <li class=""><a href="index.php">หน้าแรก</a></li>
						  <li><a href="login.php">เข้าสู่ระบบ</a></li>
				        </ul>
				      </nav><!-- #nav-menu-container -->		    		
			    	</div>
			    </div>
			  </header><!-- #header -->	
			  <?php 
				require_once('php/connect.php');
				$id=$_GET['id'];
				$sql = "SELECT * FROM promotion where id = '".$id."'";
				$result = $conn->query($sql) or die($conn->error);
				$Num_Rows=mysqli_num_rows($result);
				$row = $result->fetch_assoc()		
			?>
			<section class="generic-banner relative">	
				<div class="container">
					<div class="row height align-items-center justify-content-center">
						<div class="col-lg-10">
							<div class="generic-banner-content">
								<img class="img-fluid d-block mx-auto" src="pages/promotion/promotion/<?php echo $row['img']  ?>"  alt="">
							</div>							
						</div>
					</div>
				</div>
			</section>		
			<!-- End banner Area -->
		
		<!-- About Generic Start -->
		<div class="main-wrapper">
			
			<!-- Start Generic Area -->
			<section class="about-generic-area section-gap">
				<div class="container border-top-generic">
					<h3 class="about-title"><?php echo $row['name']  ?></h3>
					<b class="mb-5">	เมื่อวันที่ <storng><?php echo date_format(new DateTime($row['create_at']),"d/m/Y"); ?></storng></b>
					<div class="row">
						<div class="col-md-12 mt-2">
							<div class="img-text mr-20 mb-20">
								<?php echo $row['detail']  ?>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End Generic Start -->		
			<!-- start footer Area -->		
			<?php 
				require_once('php/connect.php');
				$sql = "SELECT * FROM contact ";
				$result = $conn->query($sql) or die($conn->error);
				$Num_Rows=mysqli_num_rows($result);


				$row = $result->fetch_assoc();		
			?>
			<footer class="footer-area section-gap" id="contact">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h4 class="text-white">About Us</h4>
								<p>
								  รับตรวจสภาพรถ ทำ พ.ร.บ. ประกันรถยนต์ ต่อภาษีรถยนต์ทุกประเภท โอน ย้าย เปลี่ยนสี เครื่องยนต์
								</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h4 class="text-white">Contact Us</h4>
								<p>
							     	<?php echo $row['address']  ?>		
								</p>
								<p class="number">
								    <?php echo $row['tel']  ?>		
								</p>
							</div>
						</div>						
						<div class="col-lg-5 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h4 class="text-white">Newsletter</h4>
								<p>You can trust us. we only send  offers, not a single spam.</p>
								<div class="d-flex flex-row" id="mc_embed_signup">
									  <form class="navbar-form" novalidate="true" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get">
									    <div class="input-group add-on">
									      	<input class="form-control" name="EMAIL" placeholder="Email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email address'" required="" type="email">
											<div style="position: absolute; left: -5000px;">
												<input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
											</div>
									      <div class="input-group-btn">
									        <button class="genric-btn"><span class="lnr lnr-arrow-right"></span></button>
									      </div>
									    </div>
									      <div class="info mt-20"></div>									    
									  </form>
								</div>
							</div>
						</div>				
					</div>
					
					<div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            <p class="footer-text m-0">Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						<div class="footer-social d-flex align-items-center">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-dribbble"></i></a>
							<a href="#"><i class="fa fa-behance"></i></a>
						</div>
					</div>
				</div>
			</footer>	
			<!-- End footer Area -->
			<?php ?>
			<!-- End footer Area -->
			<script src="index/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="index/js/vendor/bootstrap.min.js"></script>			
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
  			<script src="index/js/easing.min.js"></script>			
			<script src="index/js/hoverIntent.js"></script>
			<script src="index/js/superfish.min.js"></script>	
			<script src="index/js/jquery.ajaxchimp.min.js"></script>
			<script src="index/js/jquery.magnific-popup.min.js"></script>	
			<script src="index/js/owl.carousel.min.js"></script>			
			<script src="index/js/jquery.sticky.js"></script>
			<script src="index/js/jquery.nice-select.min.js"></script>			
			<script src="index/js/parallax.min.js"></script>	
			<script src="index/js/waypoints.min.js"></script>
			<script src="index/js/jquery.counterup.min.js"></script>
			<script src="index/js/mail-script.js"></script>				
			<script src="index/js/main.js"></script>	
			<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
		</body>
	</html>
