<?php
session_start();

require_once "ConnectDatabase/connectionDb.inc.php";
function encode($string,$key) {
  // sha1 เข้ารหัสจากการคำนวณทางคณิตศาตร์ซึ่งสูตรอยู่ไหนไม่รู้
  $strLen = strlen($string);
  // strlen นับจำนวน string
  $keyLen = strlen($key);
  // strlen นับจำนวน string
  for ($i = 0; $i < $strLen; $i++) {
    $ordStr = ord(substr($string,$i,1)); 
    // substr คือ คำสั่งตัดคำ มีค่า Parameter ที่สำคัญ 3 ค่า คือ ประโยค, เริ่มตัดคำ(จากอักษรที่เท่าไร) ,จำนวนการตัดคำ(ให้เหลือกี่คำก็ระบุตามต้องการ)
    // ord เอาอักษรเฉพาะตัวแรกแปลงเป็นเลข ASCII
    if ($j == $keyLen) { $j = 0; }
    $ordKey = ord(substr($key,$j,1));
    // substr คือ คำสั่งตัดคำ มีค่า Parameter ที่สำคัญ 3 ค่า คือ ประโยค, เริ่มตัดคำ(จากอักษรที่เท่าไร) ,จำนวนการตัดคำ(ให้เหลือกี่คำก็ระบุตามต้องการ)
    // ord เอาอักษรเฉพาะตัวแรกแปลงเป็นเลข ASCII
    $j++;
    $hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
    // dechex เปลี่ยนให้กลายเป็นเลขฐาน 16
    // base_convert แปลงค่าที่ได้จาก dechex($ordStr + $ordKey) ในเลขฐาน 16 ให้เป็นเลขฐาน 36
    // strrev กลับข้อความเช่น ME เป็น EM
    // .= เชื่อมข้อความระหว่างตัวแปร ordStr และ ordKey
  }
  return $hash;
}

	 if($_REQUEST[btnLogin]) {

			$user_name = getIsset('Username');
			$password = getIsset('Password');
      $passwordEncode = encode($password,g1e2t3);
			$chk_login = $conn->select('data_user', array('Username' => $user_name,'Password' => $passwordEncode), true);

			if ($chk_login != null) {

        $_SESSION['authen_id'] = 1;
        $_SESSION["UserID"] = $chk_login["UserID"];
        $_SESSION["Name"] = $chk_login["Name"];
        $_SESSION["Address"] = $chk_login["Address"];
        $_SESSION["Tel"] = $chk_login["Tel"];
        $_SESSION["status"] = $chk_login["status"];

						header('Location: pages/dashboard');

					} else {
					alertMassage("ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง");
			}
}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ลงชื่อเข้าใช้งาน</title>
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
     <img src="dist/img/logochangyai-128x128.png" class="img-circle elevation-2" alt="User Image">
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">ลงชื่อเข้าใช้งาน</p>

      <form>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-user"></i></span>
            </div>
            <input type="text" class="form-control" placeholder="ชื่อผู้ใช้งาน" aria-label="Username" name="Username" aria-describedby="basic-addon1">
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"> <i class="fas fa-lock"></i></span>
            </div>
            <input type="password" class="form-control" placeholder="รหัสผ่าน" aria-label="Password" name="Password" aria-describedby="basic-addon1">
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-12">
          <!-- recaptcha -->
				<div id="recaptcha-wrapper" class="text-center my-2">
					<!-- <div class="g-recaptcha d-inline-block" data-callback="recaptchaCallback" data-sitekey="6LfLWGkUAAAAAIIEo9XZRZVeZSIERI3fF3JkKP0x"></div> -->
				<div class="mb-1">
            <button type="submit" id="btn-submit" name="btnLogin" class="btn btn-primary btn-block btn-flat" value="true">เข้าสู่ระบบ</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

        <a href="#" data-toggle="modal" data-target="#myModal">ลืมรหัสผ่าน</a>

      <!-- 
      <p class="mb-0">
        <a href="register.php" class="text-center">Register a new membership</a>
      </p> -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

         <!-- Modal -->
         <div class="modal fade mt-5" id="myModal" role="dialog">
            <div class="modal-dialog">
            <form action="login.php" method="post" enctype="multipart/form-data">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">ตรวจสอบรหัสผ่าน</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                  
<!-- general form elements -->
                    <div class="col-md-12">                   
                      <div class="form-group">
                        <label>ชื่อผู้ใช้</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="usernames" placeholder="ชื่อผู้ใช้" >
                        </div>
                        <label  class="mt-2">เบอร์โทร</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="tels" placeholder="เบอร์โทร" >
                        </div>
                     </div>
                    </div>

                  </div>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary" >ตรวจสอบ</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
              </form>
            </div>
          </div>
      </div>
<?php 
    function decode($string,$key) {
      // sha1 เข้ารหัสจากการคำนวณทางคณิตศาตร์ซึ่งสูตรอยู่ไหนไม่รู้
      $strLen = strlen($string);
      // strlen นับจำนวน string
      $keyLen = strlen($key);
      // strlen นับจำนวน string
      for ($i = 0; $i < $strLen; $i+=2) {
      // += เพิ่มค่า $i ไปอีก 2 
          $ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));  
          // substr คือ คำสั่งตัดคำ มีค่า Parameter ที่สำคัญ 3 ค่า คือ ประโยค, เริ่มตัดคำ(จากอักษรที่เท่าไร) ,จำนวนการตัดคำ(ให้เหลือกี่คำก็ระบุตามต้องการ)
          // strrev กลับข้อความเช่น ME เป็น EM
          // base_convert แปลงค่าที่ได้จาก strrev(substr($string,$i,2)) ในเลขฐาน 36 ให้เป็นเลขฐาน 16
          // hexdex เปลี่ยนให้เป็นเลขฐาน 16  
          if ($j == $keyLen) { $j = 0; }
          $ordKey = ord(substr($key,$j,1));
          // substr คือ คำสั่งตัดคำ มีค่า Parameter ที่สำคัญ 3 ค่า คือ ประโยค, เริ่มตัดคำ(จากอักษรที่เท่าไร) ,จำนวนการตัดคำ(ให้เหลือกี่คำก็ระบุตามต้องการ)
          // ord เอาอักษรเฉพาะตัวแรกแปลงเป็นเลข ASCII
          $j++;
          $hash .= chr($ordStr - $ordKey);
          // chr return ค่าจาก ASCII มาเป็นตัวอักษร
      }
      return $hash;
    }
    if($$_POST['usernames'] != ''){
  require_once('php/connect.php');  
  $username1 = $_POST['usernames'];
  $tel1 = $_POST['tels'];
  $sql3 = "SELECT * FROM data_user WHERE Username = '$username1' and Tel='$tel1'";
  $result3 = $conn->query($sql3) or die($conn->error);
	$row_cnt = $result3->num_rows;
	// echo $row_cnt;
	  if($row_cnt < 1){
        echo "<script type='text/javascript'>alert('ขอโทษค่ะ กรุณากรอกข้อมูลให้ถูกต้อง');</script>";
    }else{
      function convertPass($str)
      {
        $str1 = substr($str,0,3); 
        return "$str1--------";
      }
      require_once('php/connect.php');  
      $username = $_POST['usernames'];
      $tel = $_POST['tels'];
      $sql="SELECT Password FROM data_user Where Username = '$username' and Tel = '$tel'";
      $result = $conn->query($sql) or die($conn->error);
      $row = $result->fetch_assoc();
      $message = decode($row['Password'],g1e2t3);
      $convertpass = convertPass($message);
      echo "<script type='text/javascript'>alert('รหัสผ่าน 3 ตัวแรกของคุณคือ $convertpass');</script>";
    }
  }
?>
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- recaptcha -->
<script src="https://www.google.com/recaptcha/api.js"></script>
<script>
				$(function(){
					// global variables
					captchaResized = false;
					captchaWidth = 304;
					captchaHeight = 78;
					captchaWrapper = $('#recaptcha-wrapper');
					captchaElements = $('#rc-imageselect, .g-recaptcha');

					$(window).on('resize', function() {
						resizeCaptcha();
					});

					resizeCaptcha();
				});

				function resizeCaptcha() {
					if (captchaWrapper.width() >= captchaWidth) {
						if (captchaResized) {
							captchaElements.css('transform', '').css('-webkit-transform', '').css('-ms-transform', '').css('-o-transform', '').css('transform-origin', '').css('-webkit-transform-origin', '').css('-ms-transform-origin', '').css('-o-transform-origin', '');
							captchaWrapper.height(captchaHeight);
							captchaResized = false;
						}
					} else {
						var scale = (1 - (captchaWidth - captchaWrapper.width()) * (0.05/15));
						captchaElements.css('transform', 'scale('+scale+')').css('-webkit-transform', 'scale('+scale+')').css('-ms-transform', 'scale('+scale+')').css('-o-transform', 'scale('+scale+')').css('transform-origin', '0 0').css('-webkit-transform-origin', '0 0').css('-ms-transform-origin', '0 0').css('-o-transform-origin', '0 0');
						captchaWrapper.height(captchaHeight * scale);
						if (captchaResized == false) captchaResized = true;
					}
				}
				// resizeCaptcha();

				function recaptchaCallback () {
					$('#btn-submit').removeAttr('disabled');
				}
			</script>

</body>
</html>
