 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>Document</title>
 </head>
 <body>
     
 <?php
 // Symmetric Encryption เข้ารหัสแบบสมมาตร ใช้ key ตัวเดียวกัน
function encode($string,$key) {
    // sha1 เข้ารหัสจากการคำนวณทางคณิตศาตร์ซึ่งสูตรอยู่ไหนไม่รู้
    $strLen = strlen($string);
    // strlen นับจำนวน string
    $keyLen = strlen($key);
    // strlen นับจำนวน string
    for ($i = 0; $i < $strLen; $i++) {
        $ordStr = ord(substr($string,$i,1)); 
        // substr คือ คำสั่งตัดคำ มีค่า Parameter ที่สำคัญ 3 ค่า คือ ประโยค, เริ่มตัดคำ(จากอักษรที่เท่าไร) ,จำนวนการตัดคำ(ให้เหลือกี่คำก็ระบุตามต้องการ)
        // ord เอาอักษรเฉพาะตัวแรกแปลงเป็นเลข ASCII
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        // substr คือ คำสั่งตัดคำ มีค่า Parameter ที่สำคัญ 3 ค่า คือ ประโยค, เริ่มตัดคำ(จากอักษรที่เท่าไร) ,จำนวนการตัดคำ(ให้เหลือกี่คำก็ระบุตามต้องการ)
        // ord เอาอักษรเฉพาะตัวแรกแปลงเป็นเลข ASCII
        $j++;
        $hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
        // dechex เปลี่ยนให้กลายเป็นเลขฐาน 16
        // base_convert แปลงค่าที่ได้จาก dechex($ordStr + $ordKey) ในเลขฐาน 16 ให้เป็นเลขฐาน 36
        // strrev กลับข้อความเช่น ME เป็น EM
        // .= เชื่อมข้อความระหว่างตัวแปร ordStr และ ordKey
    }
    return $hash;
}

function decode($string,$key) {
    // sha1 เข้ารหัสจากการคำนวณทางคณิตศาตร์ซึ่งสูตรอยู่ไหนไม่รู้
    $strLen = strlen($string);
    // strlen นับจำนวน string
    $keyLen = strlen($key);
    // strlen นับจำนวน string
    for ($i = 0; $i < $strLen; $i+=2) {
    // += เพิ่มค่า $i ไปอีก 2 
        $ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));  
        // substr คือ คำสั่งตัดคำ มีค่า Parameter ที่สำคัญ 3 ค่า คือ ประโยค, เริ่มตัดคำ(จากอักษรที่เท่าไร) ,จำนวนการตัดคำ(ให้เหลือกี่คำก็ระบุตามต้องการ)
        // strrev กลับข้อความเช่น ME เป็น EM
        // base_convert แปลงค่าที่ได้จาก strrev(substr($string,$i,2)) ในเลขฐาน 36 ให้เป็นเลขฐาน 16
        // hexdex เปลี่ยนให้เป็นเลขฐาน 16  
        if ($j == $keyLen) { $j = 0; }
        $ordKey = ord(substr($key,$j,1));
        // substr คือ คำสั่งตัดคำ มีค่า Parameter ที่สำคัญ 3 ค่า คือ ประโยค, เริ่มตัดคำ(จากอักษรที่เท่าไร) ,จำนวนการตัดคำ(ให้เหลือกี่คำก็ระบุตามต้องการ)
        // ord เอาอักษรเฉพาะตัวแรกแปลงเป็นเลข ASCII
        $j++;
        $hash .= chr($ordStr - $ordKey);
        // chr return ค่าจาก ASCII มาเป็นตัวอักษร
    }
    return $hash;
}

$entext=$_POST['entext'];
$enkey=$_POST['enkey'];
$detext=$_POST['detext'];
$dekey=$_POST['dekey'];

?>
<form action="en.php" method="post">
    <fieldset style="width:70%">
        <legend>เข้ารหัส</legend>
        Plain text <input type="text" name="entext" value="<?php echo $entext ?>">
        Key <input type="text" name="enkey" value="<?php echo $enkey ?>">
        Out put <input type="text" value="<?php echo encode($entext,$enkey); ?>">
    </fieldset>
    <fieldset style="width:70%">
        <legend>ถอดรหัส</legend>
        Cipher text <input type="text" name="detext" value="<?php echo $detext ?>">
        Key <input type="text" name="dekey" value="<?php echo $dekey ?>">
        Out put <input type="text" value="<?php echo decode($detext,$dekey); ?>">
    </fieldset>
    <button type="submit"  id="myBtn" > ดำเนินการ </button>
</form>
 </body>
 </html>
