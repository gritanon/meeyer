<?php
    session_start();

    // header('meta http-equiv="Content-Type" content="text/html; charset=utf-8 "');

    require_once "../../ConnectDatabase/connectionDb.inc.php";
    /** Excel */
    include '../../Classes/PHPExcel/IOFactory.php';

    $cmd = getIsset("__cmd");

    date_default_timezone_set('Asia/Bangkok');
    $dateNow = date("Y-m-d");
    $timeNow = date("H:i");

    $namedDataArray = array();

    if ($cmd == 'excel'){
      $total = 0;
      if ($_FILES['_file']['tmp_name'] != '') {
          $inputFileName = $_FILES['_file']['tmp_name'];

          $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
          $objReader = PHPExcel_IOFactory::createReader($inputFileType);
          $objReader->setReadDataOnly(true);
          $objPHPExcel = $objReader->load($inputFileName);

          $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
          $highestRow = $objWorksheet->getHighestRow();
          $highestColumn = $objWorksheet->getHighestColumn();

          $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
          $headingsArray = $headingsArray[1];

          $r = -1;

          $namedDataArray = array();
          for ($row = 2; $row <= $highestRow; ++$row) {
              $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
              if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                  ++$r;
                  foreach($headingsArray as $columnKey => $columnHeading) {
                      $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];

                      $total = $total + 1;
                  }
              }
          }

      }else{
          alertMassage("โปรดเลือกไฟล์");
      }
    }

    if ($cmd == 'save'){
      for($i=0;$i<=getIsset(countTable);$i++)
      {
        if ($_POST["txtlicense".$i] != "" && $_POST["txtprovince_license".$i] != "" && $_POST["txtFName".$i] != "")
        {
          // เช็คข้อมูลลูกค้า
          $tbl_cus = $conn->select('customer', array('FName' => $_POST["txtFName".$i] , 'LName' => $_POST["txtLName".$i]), true);

          //เช็คข้อมูลรถ
          $tbl_car = $conn->select('car', array('license' => $_POST["txtlicense".$i] , 'province_license' => $_POST["txtprovince_license".$i]), true);

          //ถ้าไม่มีข้อมูลลูกค้าและข้อมูลรถ
          if ($tbl_cus == null && $tbl_car == null){

            $valueCus = array(
                "FName"=>$_POST["txtFName".$i]
               ,"LName"=>$_POST["txtLName".$i]
               ,"Tel"=>$_POST["txtTel".$i]
               ,"Address"=>$_POST["txtAddress".$i]
               ,"district"=>$_POST["txtdistrict".$i]
               ,"amphoe"=>$_POST["txtamphoe".$i]
               ,"province"=>$_POST["txtprovince".$i]
               ,"zipcode"=>$_POST["txtzipcode".$i]
            );

            if($conn->create("customer",$valueCus)){
              $lastID = $conn->getLastInsertId();

              $valueCar = array(
                  "cusID"=>$lastID
                 ,"license"=>$_POST["txtlicense".$i]
                 ,"province_license"=>$_POST["txtprovince_license".$i]
                 ,"registration"=>$_POST["txtregistrations".$i]
                 ,"typecar"=>$_POST["txttypecar".$i]
                 ,"brand"=>$_POST["txtbrand".$i]
                 ,"generation"=>$_POST["txtgeneration".$i]
                 ,"body_number"=>$_POST["txtbody_number".$i]
                 ,"serial_number"=>$_POST["txtserial_number".$i]
                 ,"gas_number"=>$_POST["txtgas_number".$i]
                 ,"fuel_type"=>$_POST["txtfuel_type".$i]
                 ,"capacity"=>$_POST["txtcapacity".$i]
              );

              if($conn->create("car",$valueCar)){
              }
           }


          }else{
            //ถ้ามีข้อมุลลูกค้า
            if ($tbl_cus != null){
                //เช็คข้อมูลรถว่ามีข้อมูลรถของลูกค้าคนนี้หรือยัง
                $Fname = $_POST["txtFName".$i];
                $LName = $_POST["txtLName".$i];
                $province_license = $_POST["txtprovince_license".$i];
                $license= $_POST["txtlicense".$i];

                //เช็คข้อมูลลูกค้ากับข้อมูลรถ
                $sql = "SELECT c.id, c.FName, c.LName, ca.province_license, ca.license from customer c inner join car ca on c.id = ca.cusID where c.FName = '$Fname' and c.LName = '$LName' and province_license = '$province_license' and license = '$license'";
                $select_all = $conn->queryRaw($sql);
                $total = sizeof($select_all);

                if ($total == 0){

                  //ถ้าไม่มีข้อมูลรถของลูกค้าก็เพิ่ม
                  $valueCar = array(
                      "cusID"=>$tbl_cus["id"]
                     ,"license"=>$_POST["txtlicense".$i]
                     ,"province_license"=>$_POST["txtprovince_license".$i]
                     ,"registration"=>$_POST["txtregistrations".$i]
                     ,"typecar"=>$_POST["txttypecar".$i]
                     ,"brand"=>$_POST["txtbrand".$i]
                     ,"generation"=>$_POST["txtgeneration".$i]
                     ,"body_number"=>$_POST["txtbody_number".$i]
                     ,"serial_number"=>$_POST["txtserial_number".$i]
                     ,"gas_number"=>$_POST["txtgas_number".$i]
                     ,"fuel_type"=>$_POST["txtfuel_type".$i]
                     ,"capacity"=>$_POST["txtcapacity".$i]
                  );

                  if($conn->create("car",$valueCar)){
                  }
                }
            }
          }
        }
      }

        alertMassage("อัพโหลดข้อมูลเสร็จเรียบร้อยแล้ว");
    }

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบบริหารจัดการร้าน ตรอ. ช่างใหญ่เซอร์วิส</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php include_once('../includes/check_sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <!-- <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../dashboard">ข้อมูลลูกค้า</a></li>
              <li class="breadcrumb-item transferive">ข้อมูลลูกค้า</li>
            </ol> -->
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <form class="form-horizontal" method="post" enctype="multipart/form-data">
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title d-inline-block">Import File</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <div class="row form-group">
          <div class="col col-md-3" >
              <label for="text-input" class=" form-control-label">อัพโหลดไฟล์</label>
            </div>
            <div class="col-12 col-md-9">
              <input type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" name="_file"/>
              <button type="submit" class="btn btn-primary" name="__cmd" value="excel">อัพโหลดไฟล์</button>
          </div>
          </div>
            <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover" name="_table" id="myTable">
              <thead>
                  <tr>
                      <th>ชื่อ</th>
                      <th>นามสกุล</th>
                      <th>เบอร์โทร</th>
                      <th>ที่อยู่</th>
                      <th>ตำบล</th>
                      <th>อำเภอ</th>
                      <th>จังหวัด</th>
                      <th>รหัส ปณ.</th>
                      <th>ทะเบียนรถ</th>
                      <th>จังหวัด</th>
                      <th>วันที่จดทะเบียน</th>
                      <th>รย.</th>
                      <th>ยี่ห้อรถ</th>
                      <th>รุ่น</th>
                      <th>หมายเลขตัวถัง</th>
                      <th>หมายเลขเครื่อง</th>
                      <th>หมายเลขถังแก๊ส</th>
                      <th>ชนิดเชื้อเพลิง</th>
                      <th>ขนาดเครื่องยนต์ (ซีซี)</th>
                  </tr>
              </thead>
              <tbody>

            <?php
                $index = 0;
                foreach ($namedDataArray as $result) {
                    $index++;
                    ?>

                  <tr>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtFName<?php echo $index ?>" value="<?php echo $result['FName']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtLName<?php echo $index ?>" value="<?php echo $result['LName']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtTel<?php echo $index ?>" value="<?php echo $result['Tel']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtAddress<?php echo $index ?>" value="<?php echo $result['Address']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtdistrict<?php echo $index ?>" value="<?php echo $result['district']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtamphoe<?php echo $index ?>" value="<?php echo $result['amphoe']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtprovince<?php echo $index ?>" value="<?php echo $result['province']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtzipcode<?php echo $index ?>" value="<?php echo $result['zipcode']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtlicense<?php echo $index ?>" value="<?php echo $result['license']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtprovince_license<?php echo $index ?>" value="<?php echo $result['province_license']; ?>" readonly></td>
                      <?php
                        $date = convertDate($result['registration']);
                        $dateThtai = convertDateThai($date);
                      ?>
                      <td hidden><input style="background-color:transparent;border:none;" type="text" name="txtregistrations<?php echo $index ?>" value="<?php echo $date; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtregistration<?php echo $index ?>" value="<?php echo $dateThtai; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txttypecar<?php echo $index ?>" value="<?php echo $result['typecar']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtbrand<?php echo $index ?>" value="<?php echo $result['brand']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtgeneration<?php echo $index ?>" value="<?php echo $result['generation']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtbody_number<?php echo $index ?>" value="<?php echo $result['body_number']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtserial_number<?php echo $index ?>" value="<?php echo $result['serial_number']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtgas_number<?php echo $index ?>" value="<?php echo $result['gas_number']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtfuel_type<?php echo $index ?>" value="<?php echo $result['fuel_type']; ?>" readonly></td>
                      <td><input style="background-color:transparent;border:none;" type="text" name="txtcapacity<?php echo $index ?>" value="<?php echo $result['capacity']; ?>" readonly></td>
                  </tr>
                  <?php
                    }
                  ?>
              </tbody>
          </table>
          </div>
          <div class="box-footer">
            <center>
              <?php if ($cmd == 'excel') { ?>
                <button type="submit" class="btn btn-success" name="__cmd" value="save">บันทึกข้อมูล</button>
              <?php } ?>
          </center>
          </div>
        </div>

        <!-- /.card-body -->
      </div>
      <input type="text" id="countTable" name="countTable" value="<?php echo $total;?>" hidden>

      <!-- /.card -->
    </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>

</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- datepicker -->
<script src="../../plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js"></script>
<script src="../../plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>


<script>
  $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });

  function EditOnclick(id) {
    window.location = 'transferInfo.php?id=' + id;
  }

  function DelOnclick(id) {
    if(confirm('คุณต้องการลบข้อมูล ใช่ หรือ ไม่?') == true)
       window.location = 'transferInfo.php?id=' + id +'&__cmd=delete';
  }

  function receiptPrint(id) {
    window.open("receipt.php?id=" + id);
  }

  $(document).ready(function () {
      $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
           language: 'th',           //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
           thaiyear: true               //Set เป็นปี พ.ศ.
      }).datepicker("setDate", "1");  //กำหนดเป็นวันปัจุบัน
  });

</script>

</body>
</html>
