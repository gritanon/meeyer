<?php 
  session_start();
  require_once "../../ConnectDatabase/connectionDb.inc.php";

  $sql = "SELECT id_question,question,create_at
  FROM tb_question ";
  $select_all = $conn->queryRaw($sql);
  $total = sizeof($select_all);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Survey Management</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php include_once('../includes/check_sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

  <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title d-inline-block">ระบุวันที่เพื่อดูผลประเมินการใช้บริการ</h3>
        </div>
           <div class="card-body table-responsive">
          <form action="table_chart.php" method="post">
              <div class="col-md-12 my-2">
                  <div class="input-group">
                      <text class="m-1 mr-3">ตั้งแต่วันที่</text>
                      <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="fa fa-calendar"></i>
                      </span>
                      </div>
                      <input type="text" name="start_date"  value="<?php echo $_POST['start_date'] ?>" class="form-control datepicker col-2 text-center" data-date-format="ํd/m/Y">
                      <text class="m-1 mx-3">ถึงวันที่</text>
                      <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="fa fa-calendar"></i>
                      </span>
                      </div>
                      <input type="text" name="end_date" value="<?php echo $_POST['end_date'] ?>" class="form-control datepicker col-2 text-center" data-date-format="d/m/Y">
                      <input type="submit" value="ค้นหา">
                  </div>
              </div>
          </form>
        </div>
        </div>
      <!-- /.card -->


      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title d-inline-block">แบบสอบถาม</h3>
          <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#myModal">Question +</button>
          <!-- Modal -->
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg">
            <form action="create.php" method="post" enctype="multipart/form-data">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">เพิ่มเอกสาร</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                  
               <!-- general form elements -->
                    <div class="col-md-12">                   
                      <div class="form-group">
                        <label>คำถาม</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="text" class="form-control" name="question">
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary" >เพิ่ม</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
              </form>
            </div>
          </div>
      </div>
          
        <div class="card-body table-responsive">
          <table id="dataTable" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No.</th>
              <th>คำถาม</th>
              <th>Created</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php
                $index =0;
                  foreach ($select_all as $row) {
                      $index++;
              ?>
              <tr>
                <td><?php echo $index; ?></td>
                <td>                  
                    <?php echo $row["question"];?>
                </td>
                <td><?php echo convertDateThai($row['create_at'])  ?></td>
                <td>
                  <a href="form_edit.php?id=<?php echo $row['id_question'] ?>" class="btn btn-sm btn-warning text-white">
                    <i class="fas fa-edit"></i> edit
                  </a> 
                </td>
                <td>
                  <a href="#" onclick="deleteItem(<?php echo $row['id_question'] ?>);"  class="btn btn-sm btn-danger">
                    <i class="fas fa-trash-alt"></i> Delete
                  </a>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>
  
</div>

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- datepicker -->
<script src="../../plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js"></script>
<script src="../../plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>

<script>


     $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });

    function deleteItem (id_question) { 
    if( confirm('แน่ใจหรือไม่ที่จะลบสิ่งนี้') == true){
      window.location='delete.php?id='+id_question;
    }
    };

    $(document).ready(function () {
      $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
          language: 'th',
          thaiyear: true ,
      }).datepicker("setDate", "1");     
  });

    function switchedit() {
        document.getElementById("myDIVa1").style.display = "none";
        document.getElementById("myDIVb1").style.display = "";
        document.getElementById("myDIVa2").style.display = "none";
        document.getElementById("myDIVb2").style.display = "";
    }

    
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


</script>

</body>
</html>
