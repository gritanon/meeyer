<?php

session_start();
require_once "../../ConnectDatabase/connectionDb.inc.php";

$cmd = getIsset("__cmd");
$cusID = $_GET["cusID"];

$sql = "SELECT * FROM customer where id= $cusID";
$select_all = $conn->queryRaw($sql);

$index =0;
foreach ($select_all as $row) {
    $index++;

if (intval($id) > 0){
  //Update
  if ($cmd == 'save') {
    $value = array(
      "cusID"=>getIsset('cusID')
       ,"license"=>getIsset('license')
       ,"province_license"=>getIsset('province_license')
       ,"registration"=>getIsset('registration')
       ,"typecar"=>getIsset('typecar')
       ,"brand"=>getIsset('brand')
       ,"generation"=>getIsset('generation')
       ,"body_number"=>getIsset('body_number')
       ,"serial_number"=>getIsset('serial_number')
       ,"gas_number"=>getIsset('gas_number')
       ,"fuel_type"=>getIsset('fuel_type')
       ,"capacity"=>getIsset('capacity')
       ,"remark"=>getIsset('remark')
       ,"id_document"=>getIsset('id_document')
       ,"weight"=>getIsset('weight')
    );

    if ($conn->update("car", $value, array("id" => $id))) {
        alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','carList.php');
    }else{
        alertMassage("ไม่สามารถบันทึกข้อมูลได้");
    }

  }else{

    $tbl_car = $conn->select('car', array('id' => getIsset('id')), true);

    if($tbl_car != null){
      $cusID = $tbl_car["cusID"];
      $license = $tbl_car["license"];
      $province_license = $tbl_car["province_license"];
      $registration = $tbl_car["registration"];
      $typecar = $tbl_car["typecar"];
      $brand = $tbl_car["brand"];
      $generation = $tbl_car["generation"];
      $body_number = $tbl_car["body_number"];
      $serial_number = $tbl_car["serial_number"];
      $gas_number = $tbl_car["gas_number"];
      $fuel_type = $tbl_car["fuel_type"];
      $capacity = $tbl_car["capacity"];
      $id_document = $tbl_car["id_document"];
      $remark = $tbl_car["remark"];
      $weight = $tbl_car["weight"];

      $tbl_cus = $conn->select('customer', array('id' => $cusID), true);
      $FName = $tbl_cus["FName"];
      $LName = $tbl_cus["LName"];
      $Address = $tbl_cus["Address"];
      $district = $tbl_cus["district"];
      $amphoe = $tbl_cus["amphoe"];
      $province = $tbl_cus["province"];
      $zipcode = $tbl_cus["zipcode"];
      $Tel = $tbl_cus["Tel"];
    }
  }
}else{
  //Insert
  if ($cmd == 'save') {

    if (intval(getIsset('cusID')) > 0 ){
    $value = array(
      "cusID"=>getIsset('cusID')
       ,"license"=>getIsset('license')
       ,"province_license"=>getIsset('province_license')
       ,"registration"=>getIsset('registration')
       ,"typecar"=>getIsset('typecar')
       ,"brand"=>getIsset('brand')
       ,"generation"=>getIsset('generation')
       ,"body_number"=>getIsset('body_number')
       ,"serial_number"=>getIsset('serial_number')
       ,"gas_number"=>getIsset('gas_number')
       ,"fuel_type"=>getIsset('fuel_type')
       ,"capacity"=>getIsset('capacity')
       ,"id_document"=>getIsset('id_document')
       ,"remark"=>getIsset('remark')
       ,"weight"=>getIsset('weight')

    );
     if($conn->create("car",$value)){
        alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','carList.php');
     }else{
        alertMassage("ไม่สามารถบันทึกข้อมูลได้");
     }

   }else{
     alertMassage("โปรดเลือกข้อมูลลูกค้า");
   }
  }elseif ($cmd == 'savecarcheck') {

    if (intval(getIsset('cusID')) > 0 ){
    $value = array(
      "cusID"=>getIsset('cusID')
       ,"license"=>getIsset('license')
       ,"province_license"=>getIsset('province_license')
       ,"registration"=>convertToYmd(getIsset('registration'))
       ,"typecar"=>getIsset('typecar')
       ,"brand"=>getIsset('brand')
       ,"generation"=>getIsset('generation')
       ,"body_number"=>getIsset('body_number')
       ,"serial_number"=>getIsset('serial_number')
       ,"gas_number"=>getIsset('gas_number')
       ,"fuel_type"=>getIsset('fuel_type')
       ,"capacity"=>getIsset('capacity')
       ,"id_document"=>getIsset('id_document')
       ,"remark"=>getIsset('remark')
       ,"weight"=>getIsset('weight')

    );
     if($conn->create("car",$value)){
      $carID = $conn->getLastInsertId();
      alertMassageAndRedirect('ไปตรวจสภาพ','../carcheck/carcheckInfos.php?id='.$carID.'&cusID='.$cusID);
     }else{
        alertMassage("ไม่สามารถบันทึกข้อมูลได้");
     }

   }else{
     alertMassage("โปรดเลือกข้อมูลลูกค้า");
   }
  }elseif ($cmd == 'saveact') {

    if (intval(getIsset('cusID')) > 0 ){
    $value = array(
      "cusID"=>getIsset('cusID')
       ,"license"=>getIsset('license')
       ,"province_license"=>getIsset('province_license')
       ,"registration"=>getIsset('registration')
       ,"typecar"=>getIsset('typecar')
       ,"brand"=>getIsset('brand')
       ,"generation"=>getIsset('generation')
       ,"body_number"=>getIsset('body_number')
       ,"serial_number"=>getIsset('serial_number')
       ,"gas_number"=>getIsset('gas_number')
       ,"fuel_type"=>getIsset('fuel_type')
       ,"capacity"=>getIsset('capacity')
       ,"id_document"=>getIsset('id_document')
       ,"remark"=>getIsset('remark')
       ,"weight"=>getIsset('weight')

    );
     if($conn->create("car",$value)){
        $carID = $conn->getLastInsertId();
       alertMassageAndRedirect('ไปพรบ','../act/actInfos.php?id='.$carID.'&cusID='.$cusID);
     }else{
        alertMassage("ไม่สามารถบันทึกข้อมูลได้");
     }

   }else{
     alertMassage("โปรดเลือกข้อมูลลูกค้า");
   }
  }
}

if($cmd == 'delete'){
  //delete
    if($conn->delete("car",array("id"=>$id))){
        alertMassageAndRedirect('ลบสำเร็จ','carList.php');
    }else{
        alertMassage("ลบไม่สำเร็จ");
    }
}


?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบบริหารจัดการร้าน ตรอ. ช่างใหญ่เซอร์วิส</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
  <!-- autocomplete list -->
  <link rel="stylesheet" href="../../plugins/insurance/css/jquery-ui-1.10.2.custom.css" />
  <script src="../../plugins/insurance/js/jquery-1.9.1.js"></script>
  <script src="../../plugins/insurance/js/jquery-ui-1.10.2.custom.min.js"></script>
</head>
<body class="hold-transition sidebar-mini"  onload="addCustomer('<?php echo $row['id']; ?>'
                          ,'<?php echo $row['FName']; ?>'
                          ,'<?php echo $row['LName']; ?>'
                          ,'<?php echo $row['Tel']; ?>'
                          ,'<?php echo $row['district']; ?>'
                          ,'<?php echo $row['amphoe']; ?>'
                          ,'<?php echo $row['province']; ?>'
                          ,'<?php echo $row['zipcode']; ?>'
                          ,'<?php echo $row['Address']; ?>')">
<!-- Site wrapper -->
<?php
                                       }
                                   ?>
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php include_once('../includes/sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>ข้อมูลรถ</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../customer">ข้อมูลรถ</a></li>
              <li class="breadcrumb-item active">เพิ่มข้อมูลรถ</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <form  name="frmMain" OnSubmit="return chkString();">
      <div class="container-fluid">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">เพิ่มข้อมูลรถ</h3>
          </div>
          <div class="col-12 table-responsive card-body">
            <b class="">รายละเอียดข้อมูลเจ้าของรถ</b>
            <hr>
            <div class="card-body">
              <input type="hidden" name="id" value="<?php echo $id ?>">
              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">ชื่อ</label>
                <div class="col-8">
                  <input type="hidden" id="CusID"  placeholder="ชื่อ" class="form-control" name="cusID" value="<?php echo $cusID ?>" hidden>
                  <input type="text" id="CusFName" class="form-control" placeholder="ชื่อ" name="FName" value="<?php echo $FName; ?>" readonly required>
                </div>
                <div class="col-2">
                  <button type="button" class="btn btn-default mb-1" data-toggle="modal" data-target="#mediumModal">
                   เพิ่มข้อมูลลูกค้า
                  </button>
               </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">นามสกุล</label>
                <div class="col-10">
                  <input type="text" id="CusLName"  class="form-control" placeholder="นามสกุล" name="LName" value="<?php echo $LName; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">เบอร์โทร</label>
                <div class="col-10">
                  <input type="number" id="CusTel"  class="form-control" placeholder="เบอร์โทร" name="Tel" value="<?php echo $Tel; ?>" readonly>
                </div>
              </div>


              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">ที่อยู่</label>
                <div class="col-10">
                <input type="text"  id="CusAddress" class="form-control" placeholder="ที่อยู่" name="Address" value="<?php echo $Address; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
              <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1"></label>
                <div class="col-5">
                <input type="text"  id="Cusdistrict" class="form-control" placeholder="ตำบล / แขวง" name="district" value="<?php echo $district; ?>" readonly>
                </div>
                <div class="col-5">
                <input type="text"  id="Cusamphoe" class="form-control" placeholder="อำเภอ" name="amphoe" value="<?php echo $amphoe; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
              <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1"></label>
                <div class="col-5">
                <input type="text"  id="Cusprovince" class="form-control" placeholder="จังหวัด" name="province" value="<?php echo $province; ?>" readonly>
                </div>
                <div class="col-5">
                <input type="text"  id="Cuszipcode" class="form-control" placeholder="รหัสไปรษณีย์" name="zipcode" value="<?php echo $zipcode; ?>" readonly>
                </div>
              </div>

          </div>

          <hr>
            <b>รายละเอียดรถ</b>
          <hr>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ทะเบียนรถ</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ทะเบียนรถ" name="license" value="<?php echo $license; ?>" >
              </div>
            </div>

            <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">จังหวัด</label>
                <div class="col-6">
                <?php
                      require_once('../../php/connect.php');
                  		$comma = '';
                      $allEmp = '';
                      $sql2="SELECT province_id,province_name  FROM province_th ORDER BY province_name ASC";
                      $resultin = $conn->query($sql2) or die($conn->error);
                      while($row = $resultin->fetch_assoc()) {
                        $allEmp .= $comma.'{value: "'.$row['province_id'].'",label: "'.$row['province_name'].'"}';
                        if($comma==='') $comma = ',';
                      }
                      //การใช้งานจริง ส่วนนี้จะถูกเขียนเป็นไฟล์ .js เพื่อเรียกใช้ใน javascript
                      $allEmp = '['. $allEmp . ']';
                    ?>
                <input id="province_name"  class="form-control" placeholder="จังหวัด" name="province_license"  value="<?php echo $province_license;?>">
                <input type="hidden" id="project-id" />
                </div>
                <div class="col-2">
                  <button type="button" class="btn btn-default mb-1" data-toggle="modal" data-target="#mediumModal2">
                   เพิ่มตัวเลือก
                  </button>
               </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">วันที่จดทะเบียน</label>
              <div class="col-8">
                <input type="date" class="form-control" id="registration" name="registration" value="<?php echo $registration; ?>" >
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">รย.</label>
              <div class="col-8">
                <select id="select" class="form-control" name="typecar" >
                 <option value="">โปรดเลือก รย.</option>
                 <option value="รย.1" <?php echo ('รย.1'==  $typecar) ? ' selected="selected"' : '';?>>รย.1</option>
                 <option value="รย.2" <?php echo ('รย.2'==  $typecar) ? ' selected="selected"' : '';?>>รย.2</option>
                 <option value="รย.3" <?php echo ('รย.3'==  $typecar) ? ' selected="selected"' : '';?>>รย.3</option>
                 <option value="รย.12" <?php echo ('รย.12'==  $typecar) ? ' selected="selected"' : '';?>>รย.12</option>
                  <option value="อื่น ๆ" <?php echo ('อื่น ๆ'==  $typecar) ? ' selected="selected"' : '';?>>อื่น ๆ</option>
               </select>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ยี่ห้อรถ</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ยี่ห้อรถ" name="brand" value="<?php echo $brand; ?>" >
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">รุ่น</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="รุ่น" name="generation" value="<?php echo $generation; ?>" >
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขตัวถัง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขตัวถัง" name="body_number" id="body_number" onkeyup="strauto2()" value="<?php echo $body_number; ?>" >
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขเครื่อง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขเครื่อง" name="serial_number" id="serial_number" onkeyup="strauto()" value="<?php echo $serial_number; ?>" >
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขถังแก๊ส</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขถังแก๊ส" name="gas_number" value="<?php echo $gas_number; ?>">
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ชนิดเชื่อเพลิง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ชนิดเชื่อเพลิง" name="fuel_type" value="<?php echo $fuel_type; ?>" >
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ขนาดเครื่องยนต์ (ซีซี)</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ขนาดเครื่องยนต์" name="capacity" value="<?php echo $capacity; ?>" >
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">น้ำหนัก</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="น้ำหนัก" name="weight" value="<?php echo $weight; ?>" required>
                </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">เลขที่เอกสาร</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="เลขที่เอกสาร" name="id_document" value="<?php echo $id_document; ?>" >
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเหตุ</label>
              <div class="col-8">
                <textarea class="form-control" name="remark" rows="3" placeholder="หมายเหตุ"><?php echo $remark; ?></textarea>
              </div>
            </div>

            <div class="form-group">
             <center>
                 <button type="submit" class="btn btn-primary" name="__cmd" value="save">ตกลง</button>
                 <button type="submit" class="btn btn-primary" name="__cmd" value="savecarcheck">บันทึกและตรวจสภาพ</button>
                 <button type="submit" class="btn btn-primary" name="__cmd" value="saveact">บันทึกและทำพรบ.</button>
                 <button type="reset" class="btn btn-default" onclick="cancelOnclick()">ยกเลิก</button>
               </center>
           </div>

        <!-- /.card -->
      </div><!-- /.container-fluid -->
      </form>
    </section>
    <!-- /.content -->
  </div>

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>

</div>
<!-- ./wrapper -->

<!-- Modal Customer -->
      <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="mediumModalLabel">ข้อมูลลูกค้า</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">

                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                              <th>ลำดับ</th>
                              <th>ชื่อ</th>
                              <th>นามสกุล</th>
                              <th>เบอร์โทร</th>
                              <th>เลือก</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php

                                      $index =0;
                                              foreach ($select_all as $row) {
                                                  $index++;

                                                  ?>
                        <tr>
                          <td><?php echo $index; ?></td>
                          <td><?php echo $row['FName']; ?></td>
                          <td><?php echo $row['LName']; ?></td>
                          <td><?php echo $row['Tel']; ?></td>
                          <td align="center"> <button type="button"  class="btn btn-info" data-dismiss="modal" onclick="addCustomer('<?php echo $row['id']; ?>'
                          ,'<?php echo $row['FName']; ?>'
                          ,'<?php echo $row['LName']; ?>'
                          ,'<?php echo $row['Tel']; ?>'
                          ,'<?php echo $row['district']; ?>'
                          ,'<?php echo $row['amphoe']; ?>'
                          ,'<?php echo $row['province']; ?>'
                          ,'<?php echo $row['zipcode']; ?>'
                          ,'<?php echo $row['Address']; ?>')" >เลือก</button>
                             </td>
                          </tr>

                            <?php
                                       }
                                   ?>

                        </tbody>
                    </table>

                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                  </div>
              </div>
          </div>
      </div>
      <!-- /.modal -->
           <!-- Modal Customer -->
      <div class="modal fade" id="mediumModal2" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
          <form action="../autocomplete/add_province.php" method="post">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="mediumModalLabel">เพิ่มจังหวัด</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                  <div class="row">

                  <!-- general form elements -->
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>ชื่อจังหวัด</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="text" class="form-control" name="name">
                            </div>
                          </div>
                        </div>
                      </div>

                  </div>
                  <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" >เพิ่ม</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                  </div>
              </div>
              </form>
          </div>
      </div>
      <!-- /.modal -->

<!-- autocomplete -->
<script type="text/javascript" src="../../plugins/insurance/themes/js/allEmpData.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- datepicker -->
<script src="../../plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js"></script>
<script src="../../plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js"></script>



<script>
  $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

<script>
  $(document).ready(function () {

    var date_input=$('input[name="registration"]'); //our date input has the name "date"
         var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
         var options={
          format: 'yyyy-mm-dd',
           container: container,
           todayHighlight: true,
           autoclose: true,
           language: 'th'           //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
  
         };
         date_input.datepicker(options);

      $('#dataTables-example').DataTable({
          "oLanguage": {
              "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
              "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
              "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
              "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
              "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
              "sSearch": "ค้นหา :",
              "oPaginate": {
                  "sFirst": "เิริ่มต้น",
                  "sPrevious": "ก่อนหน้า",
                  "sNext": "ถัดไป",
                  "sLast": "สุดท้าย"
              }
          },
            responsive: true
        });
  });

  function addCustomer(id,FName,LName,Tel,Address,district,amphoe,province,zipcode) {
        document.getElementById("CusID").value = id;
        document.getElementById("CusFName").value = FName;
        document.getElementById("CusLName").value = LName;
        document.getElementById("CusAddress").value = Address;
        document.getElementById("Cusdistrict").value = district;
        document.getElementById("Cusamphoe").value = amphoe;
        document.getElementById("Cusprovince").value = province;
        document.getElementById("Cuszipcode").value = zipcode;
        document.getElementById("CusTel").value = Tel;
    }

  function cancelOnclick() {
   window.location = 'carList.php';
  }

  function strauto(){
  var text = document.getElementById('serial_number').value;  
  var str = new String ( text );
  var str = str.toUpperCase();
    document.getElementById('serial_number').value = str;
  }

  function strauto2(){
  var text = document.getElementById('body_number').value;  
  var str = new String ( text );
  var str = str.toUpperCase();
    document.getElementById('body_number').value = str;
  }

  $(function() {
    //ถ้าใช้งานจริง ส่วนนี้จะถูกเขียนขึ้น เป็นไฟล์ .js เมื่อมีการเพิ่ม/แก้ไข ข้อมูลสมาชิก
    var autoCompleteData = <?php echo $allEmp?>;
    //--

    if(!autoCompleteData) var autoCompleteData = new Array();
    $( "#province_name" ).autocomplete({
      minLength: 0,
      source: autoCompleteData,
      focus: function( event, ui ) {
      $( "#province_name" ).val( ui.item.label );
      return false;
      },
      select: function( event, ui ) {
      $( "#province_name" ).val( ui.item.label );
      $( "#project-id" ).val( ui.item.value );
      return false;
      }
    })
    .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li>" )
      .append( "<a>" + item.label + "</a>" )
      .appendTo( ul );
    };
    });

  function chkString(){
      if(document.frmMain.FName.value.length  == "")
      {
      alert('กรุณาเพิ่มข้อมูลลูกค้า');
      return false;
      }
     
      if(document.frmMain.license.value.length  == "")
      {
      alert('กรุณาใส่ทะเบียนรถ');
      return false;
      }
      
      if(document.frmMain.province_license.value.length  == "")
      {
      alert('กรุณาใส่จังหวัดในทะเบียนรถ');
      return false;
      }
      
      if(document.frmMain.registration.value.length  == "")
      {
      alert('กรุณาใส่วันที่จดทะเบียน');
      return false;
      }

      if(document.frmMain.typecar.value.length  == "")
      {
      alert('กรุณาระบุ รย. รถ');
      return false;
      }
  }


</script>

</body>
</html>
