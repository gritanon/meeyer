<?php

session_start();
require_once "../../ConnectDatabase/connectionDb.inc.php";

$id = getIsset("id");
$cmd = getIsset("__cmd");

$sql = "SELECT * FROM customer";
$select_all = $conn->queryRaw($sql);

$sql10 = "SELECT a.id,a.cusID,a.carID,a.Date,a.type,a.Price,a.startDate,a.endDate,c.license,c.province_license
FROM act a inner join car c on a.carID = c.id WHERE a.cusID='$id'";
$select_act = $conn->queryRaw($sql10);
$total_act = sizeof($select_act);

$sql2 = "SELECT cc.id,cc.cusID,cc.carID,cc.Date,cc.ConditionResults,cc.Remark,cc.Price,c.license,c.province_license
FROM car_check cc inner join car c on cc.carID = c.id WHERE cc.cusID='$id'";
$select_checkcar = $conn->queryRaw($sql2);
$total_checkcar = sizeof($select_checkcar);

$sql3 = "SELECT ch.id,ch.cusID,ch.carID,ch.Date,ch.license_new,ch.province_license_new,ch.remark,c.license,c.province_license
FROM change_car ch inner join car c on ch.carID = c.id inner join change_detail cd on cd.changeid = ch.id WHERE ch.cusID='$id'";
$select_change = $conn->queryRaw($sql3);
$total_change = sizeof($select_change);

$sql4 = "SELECT t.id,t.cusID,t.carID,t.Date,t.total,t.endDate,c.license,c.province_license
FROM tax t inner join car c on t.carID = c.id WHERE t.cusID='$id'";
$select_tax = $conn->queryRaw($sql4);
$total_tax = sizeof($select_tax);

$sql5 = "SELECT i.id,i.cusID,i.carID,i.Date,i.premium,i.endDate,i.type,i.insurance,c.license,c.province_license
FROM insurance i inner join car c on i.carID = c.id WHERE i.cusID='$id'";
$select_insurance = $conn->queryRaw($sql5);
$total_insurance = sizeof($select_insurance);

$sql6 = "SELECT tr.id,tr.cusID,tr.carID,tr.Date,tr.RFName,tr.RLName,tr.RTel,tr.RAddress,tr.Rdistrict,tr.Ramphoe,tr.Rprovince,tr.Rzipcode,c.license,c.province_license
FROM transfer tr inner join car c on tr.carID = c.id inner join transfer_detail td on td.transferid = tr.id WHERE tr.cusID='$id'";
$select_transfer = $conn->queryRaw($sql6);
$total_transfer = sizeof($select_transfer);

$sql7 = "SELECT m.id,m.cusID,m.carID,m.Date,m.source,m.destination,c.license,c.province_license
FROM move m inner join car c on m.carID = c.id inner join move_detail md on md.moveid = m.id WHERE m.cusID='$id'";
$select_move = $conn->queryRaw($sql7);
$total_move = sizeof($select_move);

$sql8 = "SELECT c.id,c.cusID,c.body_number,c.id_document,c.license,c.province_license,c.remark
FROM car c WHERE c.cusID='$id'";
$select_car = $conn->queryRaw($sql8);
$total_car = sizeof($select_car);

if (intval($id) > 0){
  //Update
  if ($cmd == 'save') {
    $value = array(
      "cusID"=>getIsset('cusID')
       ,"license"=>getIsset('license')
       ,"province_license"=>getIsset('province_license')
       ,"registration"=>getIsset('registration')
       ,"typecar"=>getIsset('typecar')
       ,"brand"=>getIsset('brand')
       ,"generation"=>getIsset('generation')
       ,"body_number"=>getIsset('body_number')
       ,"serial_number"=>getIsset('serial_number')
       ,"gas_number"=>getIsset('gas_number')
       ,"fuel_type"=>getIsset('fuel_type')
       ,"capacity"=>getIsset('capacity')
       ,"remark"=>getIsset('remark')
       ,"id_document"=>getIsset('id_document')
    );

    if ($conn->update("car", $value, array("id" => $id))) {
        alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','carList.php');
    }else{
        alertMassage("ไม่สามารถบันทึกข้อมูลได้");
    }

  }else{

    $tbl_car = $conn->select('car', array('id' => getIsset('id')), true);

    if($tbl_car != null){
      $cusID = $tbl_car["cusID"];
      $license = $tbl_car["license"];
      $province_license = $tbl_car["province_license"];
      $registration = $tbl_car["registration"];
      $typecar = $tbl_car["typecar"];
      $brand = $tbl_car["brand"];
      $generation = $tbl_car["generation"];
      $body_number = $tbl_car["body_number"];
      $serial_number = $tbl_car["serial_number"];
      $gas_number = $tbl_car["gas_number"];
      $fuel_type = $tbl_car["fuel_type"];
      $capacity = $tbl_car["capacity"];
      $id_document = $tbl_car["id_document"];
      $remark = $tbl_car["remark"];

      $tbl_cus = $conn->select('customer', array('id' => $cusID), true);
      $FName = $tbl_cus["FName"];
      $LName = $tbl_cus["LName"];
      $Address = $tbl_cus["Address"];
      $district = $tbl_cus["district"];
      $amphoe = $tbl_cus["amphoe"];
      $province = $tbl_cus["province"];
      $zipcode = $tbl_cus["zipcode"];
      $Tel = $tbl_cus["Tel"];
    }
  }
}else{
  //Insert
  if ($cmd == 'save') {

    if (intval(getIsset('cusID')) > 0 ){
    $value = array(
      "cusID"=>getIsset('cusID')
       ,"license"=>getIsset('license')
       ,"province_license"=>getIsset('province_license')
       ,"registration"=>getIsset('registration')
       ,"typecar"=>getIsset('typecar')
       ,"brand"=>getIsset('brand')
       ,"generation"=>getIsset('generation')
       ,"body_number"=>getIsset('body_number')
       ,"serial_number"=>getIsset('serial_number')
       ,"gas_number"=>getIsset('gas_number')
       ,"fuel_type"=>getIsset('fuel_type')
       ,"capacity"=>getIsset('capacity')
       ,"id_document"=>getIsset('id_document')
       ,"remark"=>getIsset('remark')

    );
     if($conn->create("car",$value)){
        alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','carList.php');
     }else{
        alertMassage("ไม่สามารถบันทึกข้อมูลได้");
     }

   }else{
     alertMassage("โปรดเลือกข้อมูลลูกค้า");
   }
  }
}

if($cmd == 'delete'){
  //delete
    if($conn->delete("car",array("id"=>$id))){
        alertMassageAndRedirect('ลบสำเร็จ','carList.php');
    }else{
        alertMassage("ลบไม่สำเร็จ");
    }
}

$registhai = convertDateThai($registration);

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบบริหารจัดการร้าน ตรอ. ช่างใหญ่เซอร์วิส</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php include_once('../includes/sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <form>
      <div class="container-fluid">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">ข้อมูลรถ</h3>
          </div>
          <div class="col-12 table-responsive card-body">
            <b class="">รายละเอียดข้อมูลเจ้าของรถ</b>
            <hr>
            <div class="card-body">
              <input type="hidden" name="id" value="<?php echo $id ?>">
              <div class="row form-group">
                  <label class="ml-4 mt-1 col-sm-2" for="exampleInputEmail1">&nbsp;  ชื่อ-นามสกุล</label>
                <div class="col-8">
                  <input type="hidden" id="CusID"  placeholder="ชื่อ" class="form-control" name="cusID" value="<?php echo $cusID ?>" hidden>
                  <input type="text" id="CusFName" class="form-control ml-2" placeholder="ชื่อ" name="FName" value="<?php echo $FName; ?> <?php echo $LName; ?>" >
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-4 mt-1 col-sm-2" for="exampleInputEmail1">&nbsp;  เบอร์โทร</label>
                <div class="col-8">
                  <input type="number" id="CusTel"  class="form-control ml-2" placeholder="เบอร์โทร" name="Tel" value="<?php echo $Tel; ?>" >
                </div>
              </div>
              
              <div class="row form-group">
                  <label class="ml-4 mt-1 col-sm-2" for="exampleInputEmail1">&nbsp;  ที่อยู่</label>
                <div class="col-8">
                <input type="text" class="form-control ml-2" placeholder="บ้านเลขที่" name="Address" value="<?php echo $Address; ?> ต.<?php echo $district; ?> อ.<?php echo $amphoe; ?> จ.<?php echo $province; ?> <?php echo $zipcode; ?>">
                </div>
              </div>

          </div>

          <hr>
            <b>รายละเอียดรถ</b>
          <hr>

            <div class="row form-group">
                <label class="ml-5 mt-1 col-sm-2" for="exampleInputEmail1">ทะเบียนรถ</label>
              <div class="col-3">
                <input type="text" class="form-control" placeholder="ทะเบียนรถ" name="license" value="<?php echo $license; ?> <?php echo $province_license; ?>" required>
              </div>
              <label class="ml-4 mt-1 mr-4" for="exampleInputEmail1">วันที่จดทะเบียน</label>
              <div class="col-3">
                <input type="text" class="form-control" placeholder="วันที่จดทะเบียน"  value="<?php echo $registhai; ?>" required>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mt-1 col-sm-2" for="exampleInputEmail1">รย.</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="รย." name="license" value="<?php echo $typecar; ?>" required>
              </div>
            </div>

            <div class="row form-group">
              <label class="ml-5 mt-1 col-sm-2" for="exampleInputEmail1">ยี่ห้อรถ-รุ่น</label>
              <div class="col-8">
              <input type="text" class="form-control" placeholder="ยี่ห้อรถ" name="brand" value="<?php echo $brand; ?> <?php echo $generation; ?>" required>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขตัวถัง</label>
              <div class="col-3">
                <input type="text" class="form-control" placeholder="หมายเลขตัวถัง" name="license" value="<?php echo $body_number; ?>" required>
              </div>
              <label class="ml-4 mt-1 mr-4" for="exampleInputEmail1">หมายเลขเครื่อง</label>
              <div class="col-3">
                <input type="text" class="form-control" placeholder="หมายเลขเครื่อง"  value="<?php echo $serial_number; ?>" required>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขถังแก๊ส</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขถังแก๊ส" name="gas_number" value="<?php echo $gas_number; ?>">
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ชนิดเชื่อเพลิง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ชนิดเชื่อเพลิง" name="fuel_type" value="<?php echo $fuel_type; ?>" required>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ขนาดเครื่องยนต์ (ซีซี)</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ขนาดเครื่องยนต์" name="capacity" value="<?php echo $capacity; ?>" required>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">เลขที่เอกสาร</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="เลขที่เอกสาร" name="id_document" value="<?php echo $id_document; ?>" required>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเหตุ</label>
              <div class="col-8">
                <textarea class="form-control" name="remark" rows="3" placeholder="หมายเหตุ"><?php echo $remark; ?></textarea>
              </div>
            </div>

            <hr>
        <b class="">รายละเอียดการใช้บริการ รายละเอียดการใช้บริการ</b>
        <hr>
         <a href="#checkcar" class="btn btn-primary" data-toggle="collapse" id="b1">ตรวจสภาพรถ <cl style="display:none;" id="cc"> <i class="fas fa-angle-down ml-2"></i> </cl></a>
         <a href="#act" class="btn btn-secondary" data-toggle="collapse" id="b2">พรบ.<cl style="display:none;" id="cc2"> <i class="fas fa-angle-down ml-2"></i> </cl></a>
         <a href="#tax" class="btn btn-success" data-toggle="collapse" id="b3">ภาษี<cl style="display:none;" id="cc3"> <i class="fas fa-angle-down ml-2"></i> </cl></a>
         <a href="#insurance" class="btn btn-danger" data-toggle="collapse" id="b4">ประกันภาคสมัครใจ<cl style="display:none;" id="cc4"> <i class="fas fa-angle-down ml-2"></i> </cl></a>
         <a href="#change" class="btn btn-warning" data-toggle="collapse" id="b5">เปลี่ยนทะเบียน<cl style="display:none;" id="cc5"> <i class="fas fa-angle-down ml-2"></i> </cl></a>
         <a href="#transfer" class="btn btn-info" data-toggle="collapse" id="b6">โอน<cl style="display:none;" id="cc6"> <i class="fas fa-angle-down ml-2"></i> </cl></a>
         <a href="#move" class="btn btn-light" data-toggle="collapse" id="b7">ย้ายทะเบียน<cl style="display:none;" id="cc7"> <i class="fas fa-angle-down ml-2"></i> </cl></a>
         <br><small class=" text-muted">*กรุณาเลือกบริการเพื่อดูรายการใช้บริการ</small>
        <hr>
        <div id="checkcar" class="collapse">
          <b class="text-primary">รายละเอียดการใช้บริการ ตรวจสภาพรถ</b>
          <a href="../carcheck/carcheckInfos.php?id=<?= $id; ?> & cusID=<?= $cusID; ?>" class="btn btn-primary float-right "> + เพิ่มข้อมูลตรวจสภาพรถ</a>
          <hr>
          <table id="dataTable2" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>ทะเบียนรถ</th>
                <th>จังหวัด</th>
                <th>สถานะ</th>
                <th>หมายเหตุ</th>
                <th>ราคา</th>
                <th>วันที่ทำรายการ</th>
                <th>ใบเสร็จรับเงิน</th>
                <th>แก้ไข</th>
                <th>ลบ</th>
              </tr>
              </thead>
              <tbody>
              <?php
                  $index =0;
                    foreach ($select_checkcar as $row) {
                        $index++;
                ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td >
                      <?php echo $row['license']  ?>
                  </td>
                  <td>                  
                      <?php echo $row['province_license']  ?>
                  </td>
                  <td><?php echo $row['ConditionResults']  ?></td>
                  <td><?php echo $row['Remark']  ?></td>
                  <td><?php echo $row['Price']  ?></td>
                  <td><?php echo convertDateThai($row['Date'])  ?></td>
                  <td align="center">
                    <a onclick="receiptPrint_carcheck(<?php echo $row['id']; ?>)"><i class="fas fa-print"></i></a>
                  </td>
                  <td align="center">
                     <a onclick="EditOnclick_carcheck(<?php echo $row['id']; ?>)" class="btn btn-sm btn-warning text-white"><i class="fas fa-edit"></i>Edit</a>
                  </td>
                  <td align="center">
                    <a onclick="DelOnclick_carcheck(<?php echo $row['id']; ?>)"  class="btn btn-sm btn-danger text-white"><i class="fas fa-trash-alt mr-1"> </i>Delete</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          <hr>
        </div>
        <div id="act" class="collapse">
          <b class="text-secondary">รายละเอียดการใช้บริการ พรบ.</b>
          <a href="../act/actInfos.php?id=<?= $id; ?> & cusID=<?= $cusID; ?>" class="btn btn-primary float-right "> + เพิ่มข้อมูล พรบ.</a>
          <hr>
          <table id="dataTable3" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>ทะเบียนรถ</th>
                <th>จังหวัด</th>
                <th>บริษัทที่คุ้มครอง</th>
                <th>ค่าบริการ</th>
                <th>วันที่ทำรายการ</th>
                <th>ใบเสร็จรับเงิน</th>
                <th>รายละเอียด</th>
              </tr>
              </thead>
              <tbody>
              <?php
                  $index =0;
                    foreach ($select_act as $row) {
                        $index++;
                ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td >
                      <?php echo $row['license']  ?>
                  </td>
                  <td>                  
                      <?php echo $row['province_license']  ?>
                  </td>
                  <td><?php echo $row['type']  ?></td>
                  <td><?php echo $row['Price']  ?></td>
                  <td><?php echo convertDateThai($row['Date'])  ?></td>
                  <td align="center">
                    <a onclick="receiptPrint_act(<?php echo $row['id']; ?>)"><i class="fas fa-print"></i></a>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm btn-warning text-white" onclick="EditOnclick_act(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i>รายละเอียด</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>

           <hr>
        </div>
        <div id="tax" class="collapse">
          <b class="text-success">รายละเอียดการใช้บริการ ภาษี</b>
          <a href="../tax/taxInfos.php?id=<?= $id; ?> & cusID=<?= $cusID; ?>" class="btn btn-primary float-right "> + เพิ่มข้อมูล ภาษี</a>
          <hr>
          <table id="dataTable4" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>ทะเบียนรถ</th>
                <th>วันครบกำหนดครั้งต่อไป</th>
                <th>ค่าบริการ</th>
                <th>วันที่ทำรายการ</th>
                <th>ใบเสร็จรับเงิน</th>
                <th>รายละเอียด</th>
              </tr>
              </thead>
              <tbody>
              <?php
                  $index =0;
                    foreach ($select_tax as $row) {
                        $index++;
                ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td >
                      <?php echo $row['license']  ?>  <?php echo $row['province_license']  ?>
                  </td>
                  <td><?php echo convertDateThai($row['endDate'])  ?></td>
                  <td><?php echo $row['total']  ?></td>
                  <td><?php echo convertDateThai($row['Date'])  ?></td>
                  <td align="center">
                    <a onclick="receiptPrint_tax(<?php echo $row['id']; ?>)"><i class="fas fa-print"></i></a>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm btn-warning text-white" onclick="EditOnclick_tax(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i>รายละเอียด</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          <hr>
        </div>
        <div id="insurance" class="collapse">
          <b class="text-danger">รายละเอียดการใช้บริการ ประกันภัยภาคสมัครใจ</b>
          <a href="../insurance/insuranceInfos.php?id=<?= $id; ?> & cusID=<?= $cusID; ?>" class="btn btn-primary float-right "> + เพิ่มข้อมูล ประกันภัยภาคสมัครใจ</a>
          <hr>
          <table id="dataTable5" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>ทะเบียนรถ</th>
                <th>บริษัทที่คุ้มครอง</th>
                <th>เบี้ยประกัน</th>
                <th>ทุนประกัน</th>
                <th>วันสิ้นสุดการคุ้มครอง</th>
                <th>วันที่ทำรายการ</th>
                <th>ใบเสร็จรับเงิน</th>
                <th>รายละเอียด</th>
              </tr>
              </thead>
              <tbody>
              <?php
                  $index =0;
                    foreach ($select_insurance as $row) {
                        $index++;
                ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td >
                      <?php echo $row['license']  ?>  <?php echo $row['province_license']  ?>
                  </td>
                  <td><?php echo $row['type']  ?></td>
                  <td><?php echo $row['premium']  ?></td>
                  <td><?php echo $row['insurance']  ?></td>
                  <td><?php echo convertDateThai($row['endDate'])  ?></td>
                  <td><?php echo convertDateThai($row['Date'])  ?></td>
                  <td align="center">
                    <a onclick="receiptPrint_insurance(<?php echo $row['id']; ?>)"><i class="fas fa-print"></i></a>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm btn-warning text-white" onclick="EditOnclick_insurance(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i>รายละเอียด</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          <hr>
        </div>
        <div id="change" class="collapse">
          <b class="text-warning">รายละเอียดการใช้บริการ เปลี่ยนทะเบียน</b>
          <a href="../change/changeInfos.php?id=<?= $id; ?> & cusID=<?= $cusID; ?>" class="btn btn-primary float-right "> + เพิ่มข้อมูล เปลี่ยนทะเบียน</a>
          <hr>
          <table id="dataTable6" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>ทะเบียนรถ เดิม</th>
                <th>ทะเบียนรถ ใหม่</th>
                <th>วันที่ทำรายการ</th>
                <th>ใบเสร็จรับเงิน</th>
                <th>รายละเอียด</th>
              </tr>
              </thead>
              <tbody>
              <?php
                  $index =0;
                    foreach ($select_change as $row) {
                        $index++;
                ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td >
                      <?php echo $row['license']  ?>  <?php echo $row['province_license']  ?>
                  </td>
                  <td >
                      <?php echo $row['license_new']  ?>  <?php echo $row['province_license_new']  ?>
                  </td>
                  <td><?php echo convertDateThai($row['Date'])  ?></td>
                  <td align="center">
                    <a onclick="receiptPrint_change(<?php echo $row['id']; ?>)"><i class="fas fa-print"></i></a>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm btn-warning text-white" onclick="EditOnclick_change(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i>รายละเอียด</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>

          <hr>
        </div>
        <div id="transfer" class="collapse">
          <b class="text-info">รายละเอียดการใช้บริการ การโอน</b>
          <a href="../transfer/transferInfos.php?id=<?= $id; ?> & cusID=<?= $cusID; ?>" class="btn btn-primary float-right "> + เพิ่มข้อมูล การโอน</a>
          <hr>
          <table id="dataTable7" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>ทะเบียนรถ</th>
                <th>ชื่อผู้รับ</th>
                <th>วันที่ทำรายการ</th>
                <th>ใบเสร็จรับเงิน</th>
                <th>รายละเอียด</th>
              </tr>
              </thead>
              <tbody>
              <?php
                  $index =0;
                    foreach ($select_transfer as $row) {
                        $index++;
                ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td >
                      <?php echo $row['license']  ?>  <?php echo $row['province_license']  ?>
                  </td>
                  <td >
                      <?php echo $row['RFName']  ?>  <?php echo $row['RLName']  ?>
                  </td>
                  <td><?php echo convertDateThai($row['Date'])  ?></td>
                  <td align="center">
                    <a onclick="receiptPrint_transfer(<?php echo $row['id']; ?>)"><i class="fas fa-print"></i></a>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm btn-warning text-white" onclick="EditOnclick_transfer(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i>รายละเอียด</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          <hr>
        </div>
        <div id="move" class="collapse">
          <b class="">รายละเอียดการใช้บริการ การย้ายทะเบียน</b>
          <a href="../move/moveInfos.php?id=<?= $id; ?> & cusID=<?= $cusID; ?>" class="btn btn-primary float-right "> + เพิ่มข้อมูล การย้ายทะเบียน</a>
          <hr>
          <table id="dataTable8" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No.</th>
                <th>ทะเบียนรถ</th>
                <th>ต้นทาง</th>
                <th>ปลายทาง</th>
                <th>วันที่ทำรายการ</th>
                <th>ใบเสร็จรับเงิน</th>
                <th>รายละเอียด</th>
              </tr>
              </thead>
              <tbody>
              <?php
                  $index =0;
                    foreach ($select_move as $row) {
                        $index++;
                ?>
                <tr>
                  <td><?php echo $index; ?></td>
                  <td >
                      <?php echo $row['license']  ?>  <?php echo $row['province_license']  ?>
                  </td>
                  <td >
                      <?php echo $row['source']  ?> 
                  </td>
                  <td><?php echo $row['destination']  ?></td>
                  <td><?php echo convertDateThai($row['Date'])  ?></td>
                  <td align="center">
                    <a onclick="receiptPrint_transfer(<?php echo $row['id']; ?>)"><i class="fas fa-print"></i></a>
                  </td>
                  <td align="center">
                    <a class="btn btn-sm btn-warning text-white" onclick="EditOnclick_transfer(<?php echo $row['id']; ?>)"><i class="fas fa-edit"></i>รายละเอียด</a>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
        </div>

        <!-- /.card -->
      </div><!-- /.container-fluid -->
      </form>
    </section>
    <!-- /.content -->
  </div>

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>

</div>
<!-- ./wrapper -->

<!-- Modal Customer -->
      <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="mediumModalLabel">ข้อมูลลูกค้า</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">

                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                              <th>ลำดับ</th>
                              <th>ชื่อ</th>
                              <th>นามสกุล</th>
                              <th>เบอร์โทร</th>
                              <th>เลือก</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php

                                      $index =0;
                                              foreach ($select_all as $row) {
                                                  $index++;

                                                  ?>
                        <tr>
                          <td><?php echo $index; ?></td>
                          <td><?php echo $row['FName']; ?></td>
                          <td><?php echo $row['LName']; ?></td>
                          <td><?php echo $row['Tel']; ?></td>
                          <td align="center"> <button type="button"  class="btn btn-info" data-dismiss="modal" onclick="addCustomer('<?php echo $row['id']; ?>','<?php echo $row['FName']; ?>','<?php echo $row['LName']; ?>','<?php echo $row['Tel']; ?>','<?php echo $row['Address']; ?>')" >เลือก</button>
                             </td>
                          </tr>

                            <?php
                                       }
                                   ?>

                        </tbody>
                    </table>

                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                  </div>
              </div>
          </div>
      </div>
      <!-- /.modal -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datepicker/locales/bootstrap-datepicker.th.js"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker-thai.js"></script>
<!-- datepicker -->
<script src="../../plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js"></script>
<script src="../../plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js"></script>



<script>
  $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

<script>
  $(document).ready(function () {

    var date_input=$('input[name="registration"]'); //our date input has the name "date"
         var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
         var options={
           format: 'yyyy-mm-dd',
           container: container,
           todayHighlight: true,
           autoclose: true,
           language: 'th',           //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
           thaiyear: true    
         };
         date_input.datepicker(options);

      $('#dataTables-example').DataTable({
          "oLanguage": {
              "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
              "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
              "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
              "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
              "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
              "sSearch": "ค้นหา :",
              "oPaginate": {
                  "sFirst": "เิริ่มต้น",
                  "sPrevious": "ก่อนหน้า",
                  "sNext": "ถัดไป",
                  "sLast": "สุดท้าย"
              }
          },
            responsive: true
        });
  });
  $(function () {
    $('#dataTable2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });
  $(function () {
    $('#dataTable3').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });
  $(function () {
    $('#dataTable4').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });
  $(function () {
    $('#dataTable5').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });
  $(function () {
    $('#dataTable6').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });
  $(function () {
    $('#dataTable7').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });
  $(function () {
    $('#dataTable8').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });

  function EditOnclick_act(id) {
    window.location = '../act/actInfo.php?id=' + id;
  }

  function receiptPrint_act(id) {
    window.open("../act/receipt.php?id=" + id);
  }

  function EditOnclick_carcheck(id) {
    window.location = '../carcheck/carcheckInfos.php?id=' + id;
  }

  function DelOnclick_carcheck(id) {
    if(confirm('คุณต้องการลบข้อมูล ใช่ หรือ ไม่?') == true)
       window.location = '../carcheck/carcheckInfos.php?id=' + id +'&__cmd=delete';
  }

  function receiptPrint_carcheck(id) {
    window.open("../carcheck/receipt.php?id=" + id);
  }

  function EditOnclick_change(id) {
    window.location = '../change/changeInfo.php?id=' + id;
  }

  function receiptPrint_change(id) {
    window.open("../change/receipt.php?id=" + id);
  }

  function EditOnclick_tax(id) {
    window.location = '../tax/taxInfo.php?id=' + id;
  }

  function receiptPrint_tax(id) {
    window.open("../tax/receipt.php?id=" + id);
  }

  function EditOnclick_insurance(id) {
    window.location = '../insurance/insuranceInfo.php?id=' + id;
  }

  function receiptPrint_insurance(id) {
    window.open("../insurance/receipt.php?id=" + id);
  }

  function EditOnclick_transfer(id) {
    window.location = '../transfer/transferInfo.php?id=' + id;
  }

  function receiptPrint_transfer(id) {
    window.open("../transfer/receipt.php?id=" + id);
  }

  function addCustomer(id,FName,LName,Tel,Address) {
        document.getElementById("CusID").value = id;
        document.getElementById("CusFName").value = FName;
        document.getElementById("CusLName").value = LName;
        document.getElementById("CusAddress").value = Address;
        document.getElementById("CusTel").value = Tel;
    }

  function cancelOnclick() {
   window.location = 'carList.php';
  }

  $(document).ready(function(){
    $("#b1").click(function(){
        $("#checkcar").show();
        $("#act").hide();
        $("#tax").hide();
        $("#insurance").hide();
        $("#change").hide();
        $("#transfer").hide();
        $("#move").hide();
        $("#cc").show();
        $("#cc2").hide();
        $("#cc3").hide();
        $("#cc4").hide();
        $("#cc5").hide();
        $("#cc6").hide();
        $("#cc7").hide();
    });

    $("#b2").click(function(){
       $("#checkcar").hide();
        $("#act").show();
        $("#tax").hide();
        $("#insurance").hide();
        $("#change").hide();
        $("#transfer").hide();
        $("#move").hide();
        $("#cc2").show();
        $("#cc3").hide();
        $("#cc4").hide();
        $("#cc5").hide();
        $("#cc6").hide();
        $("#cc7").hide();
        $("#cc").hide();
    });

    $("#b3").click(function(){
       $("#checkcar").hide();
        $("#act").hide();
        $("#tax").show();
        $("#insurance").hide();
        $("#change").hide();
        $("#transfer").hide();
        $("#move").hide();
        $("#cc3").show();
        $("#cc4").hide();
        $("#cc5").hide();
        $("#cc6").hide();
        $("#cc7").hide();
        $("#cc2").hide();
        $("#cc").hide();
    });

    $("#b4").click(function(){
        $("#checkcar").hide();
        $("#act").hide();
        $("#tax").hide();
        $("#insurance").show();
        $("#change").hide();
        $("#transfer").hide();
        $("#move").hide(); 
        $("#cc4").show();
        $("#cc5").hide();
        $("#cc6").hide();
        $("#cc7").hide();
        $("#cc3").hide();
        $("#cc2").hide();
        $("#cc").hide();
    });

    $("#b5").click(function(){
      $("#checkcar").hide();
        $("#act").hide();
        $("#tax").hide();
        $("#insurance").hide();
        $("#change").show();
        $("#transfer").hide();
        $("#move").hide();
       $("#cc5").show();
       $("#cc6").hide();
        $("#cc7").hide();
        $("#cc4").hide();
        $("#cc3").hide();
        $("#cc2").hide();
        $("#cc").hide();
    });

    $("#b6").click(function(){
      $("#checkcar").hide();
        $("#act").hide();
        $("#tax").hide();
        $("#insurance").hide();
        $("#change").hide();
        $("#transfer").show();
        $("#move").hide();
        $("#cc6").show();
        $("#cc7").hide();
        $("#cc5").hide();
        $("#cc4").hide();
        $("#cc3").hide();
        $("#cc2").hide();
        $("#cc").hide();
    });

    $("#b7").click(function(){
        $("#checkcar").hide();
        $("#act").hide();
        $("#tax").hide();
        $("#insurance").hide();
        $("#change").hide();
        $("#transfer").hide();
        $("#move").show();
        $("#cc7").show();
        $("#cc6").hide();
        $("#cc5").hide();
        $("#cc4").hide();
        $("#cc3").hide();
        $("#cc2").hide();
        $("#cc").hide();
    });
});

</script>

</body>
</html>
