<?php

session_start();
require_once "../../ConnectDatabase/connectionDb.inc.php";

$id = getIsset("id");
$cmd = getIsset("__cmd");

$sql = "SELECT c.id,cus.FName ,cus.LName,cus.address,cus.district,cus.amphoe,cus.province,cus.zipcode,cus.tel,c.license,c.registration,c.typecar,c.brand,c.generation,c.serial_number,c.gas_number,c.fuel_type,
c.province_license,c.body_number,cus.id as cusID,c.capacity from car c inner join customer cus on c.cusID = cus.id";
$select_all = $conn->queryRaw($sql);

if (intval($id) > 0){
  //Update
  if ($cmd == 'save') {
    $value = array(
      "cusID"=>getIsset('cusID')
      ,"carID"=>getIsset('carID')
      ,"RFName"=>getIsset('RFName')
      ,"RLName"=>getIsset('RLName')
      ,"RTel"=>getIsset('RTel')
      ,"RAddress"=>getIsset('RAddress')
      ,"Rdistrict"=>getIsset('Rdistrict')
      ,"Ramphoe"=>getIsset('Ramphoe')
      ,"Rprovince"=>getIsset('Rprovince')
      ,"Rzipcode"=>getIsset('Rzipcode')
    );

      if ($conn->update("transfer", $value, array("id" => $id))) {

            $valueCus2 = array(
              "FName"=>getIsset('RFName')
              ,"LName"=>getIsset('RLName')
              ,"Tel"=>getIsset('RTel')
              ,"Address"=>getIsset('RAddress')
              ,"district"=>getIsset('Rdistrict')
              ,"amphoe"=>getIsset('Ramphoe')
              ,"province"=>getIsset('Rprovince')
              ,"zipcode"=>getIsset('Rzipcode')
            );

        if ($conn->update("customer", $valueCus2, array("id" => getIsset('cusIDs')))) {

          // Delete
          DeleteTransferDetail($id);

          for($i=0;$i<=getIsset('countTable');$i++)
          {
              if ($_POST["txtName".$i] != "" && $_POST["txtPrice".$i] != "") {
                //insert
                InsertTransferDetail($id,$_POST["txtName".$i],$_POST["txtPrice".$i]);
              }
          }
          alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','transferList.php');
        
        }else{
          alertMassage("ไม่สามารถบันทึกข้อมูลได้");
        }
      }else{
        alertMassage("ไม่สามารถบันทึกข้อมูลได้");
      }
  }else{

    $tbl_transfer = $conn->select('transfer', array('id' => getIsset('id')), true);

    if($tbl_transfer != null){
      $cusID = $tbl_transfer["cusID"];
      $carID = $tbl_transfer["carID"];
      $Date = $tbl_transfer["Date"];
      $RFName = $tbl_transfer["RFName"];
      $RLName = $tbl_transfer["RLName"];
      $RTel = $tbl_transfer["RTel"];
      $RAddress = $tbl_transfer["RAddress"];
      $Rdistrict = $tbl_transfer["Rdistrict"];
      $Ramphoe = $tbl_transfer["Ramphoe"];
      $Rprovince = $tbl_transfer["Rprovince"];
      $Rzipcode = $tbl_transfer["Rzipcode"];

      $tbl_car = $conn->select('car', array('id' => $carID), true);
      $license = $tbl_car["license"];
      $province_license = $tbl_car["province_license"];
      $registration = convertDateThai($tbl_car["registration"]);
      $typecar = $tbl_car["typecar"];
      $brand = $tbl_car["brand"];
      $generation = $tbl_car["generation"];
      $body_number = $tbl_car["body_number"];
      $serial_number = $tbl_car["serial_number"];
      $gas_number = $tbl_car["gas_number"];
      $fuel_type = $tbl_car["fuel_type"];
      $capacity = $tbl_car["capacity"];
      $cusIDs = $tbl_car["cusID"];

      $tbl_cus = $conn->select('customer', array('id' => $cusID), true);
      $FName = $tbl_cus["FName"];
      $LName = $tbl_cus["LName"];
      $Address = $tbl_cus["Address"];
      $district = $tbl_cus["district"];
      $amphoe = $tbl_cus["amphoe"];
      $province = $tbl_cus["province"];
      $zipcode = $tbl_cus["zipcode"];
      $Tel = $tbl_cus["Tel"];

      $sql = "SELECT *  from transfer_detail WHERE transferId = $id";
      $tbl = $conn->queryRaw($sql);
      $total = sizeof($tbl);

    }
  }
}else{
  //Insert
  if ($cmd == 'save') {

      if (intval(getIsset('carID')) > 0 ){
  
            $value = array(
              "cusID"=>getIsset('cusID')
              ,"carID"=>getIsset('carID')
              ,"Date"=>convertToDateThai(Date('Y-m-d'))
              ,"RFName"=>getIsset('RFName')
              ,"RLName"=>getIsset('RLName')
              ,"RTel"=>getIsset('RTel')
              ,"RAddress"=>getIsset('RAddress')
              ,"Rdistrict"=>getIsset('Rdistrict')
              ,"Ramphoe"=>getIsset('Ramphoe')
              ,"Rprovince"=>getIsset('Rprovince')
              ,"Rzipcode"=>getIsset('Rzipcode')
            );
  
            if($conn->create("transfer",$value)){
              $lastID = $conn->getLastInsertId();
  
              $valueCus = array(
                "FName"=>getIsset('RFName')
                ,"LName"=>getIsset('RLName')
                ,"Tel"=>getIsset('RTel')
                ,"Address"=>getIsset('RAddress')
                ,"district"=>getIsset('Rdistrict')
                ,"amphoe"=>getIsset('Ramphoe')
                ,"province"=>getIsset('Rprovince')
                ,"zipcode"=>getIsset('Rzipcode')
              );
            
                if ($conn->create("customer", $valueCus, array("id" => getIsset('cusID')))) {
                  $lastcusID = $conn->getLastInsertId();

                  $valueCar = array(
                    "cusID"=>$lastcusID
                  );

                  if ($conn->update("car", $valueCar, array("id" => getIsset('carID')))) {
                      
                    for($i=0;$i<=getIsset('countTable');$i++)
                    {
                        if ($_POST["txtName".$i] != "" && $_POST["txtPrice".$i] != "") {
                          //insert
                          InsertTransferDetail($lastID,$_POST["txtName".$i],$_POST["txtPrice".$i]);
                        }
                    }
                    alertMassageAndRedirect('บันทึกข้อมูลเรียบร้อยแล้ว','transferList.php');
                  }else{
                      alertMassage("ไม่สามารถบันทึกข้อมูลได้");
                  }
                }else{
                  alertMassage("ไม่สามารถบันทึกข้อมูลได้");
              }
            }else{
              alertMassage("ไม่สามารถบันทึกข้อมูลได้");
            }
     }else{
       alertMassage("โปรดเลือกข้อมูลผู้โอน");
     }
    }
  }

if($cmd == 'delete'){
  //delete

    DeleteTransferDetail($id);
    if($conn->delete("transfer",array("id"=>$id))){
        alertMassageAndRedirect('ลบสำเร็จ','transferList.php');
    }else{
        alertMassage("ลบไม่สำเร็จ");
    }
}


?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบบริหารจัดการร้าน ตรอ. ช่างใหญ่เซอร์วิส</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
  <!-- address -->
  <link rel="stylesheet" href="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dist/jquery.Thailand.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php include_once('../includes/check_sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>ข้อมูลการโอน</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../customer">ข้อมูลการโอน</a></li>
              <li class="breadcrumb-item transferive">เพิ่มข้อมูลการโอน</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <form name="frmMain" OnSubmit="return chkString();" method="post" id="demo1" class="demo" style="display:none;" autocomplete="off" uk-grid >
      <div class="container-fluid">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">เพิ่มข้อมูลการโอน</h3>
          </div>
          <div class="col-12 table-responsive card-body">
            <b class="">รายละเอียดข้อมูลเจ้าของรถ (ผู้โอน)</b>
            <hr>
            <div class="card-body">
              <input type="hidden" name="id" value="<?php echo $id ?>">
              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">ชื่อ</label>
              <?php if (intval($id) == 0) { ?>
                <div class="col-8">
                <?php } else { ?>
                <div class="col-10">
                <?php } ?>
                  <input type="hidden" id="CusID"  placeholder="ชื่อ" class="form-control" name="cusID" value="<?php echo $cusID ?>" hidden>
                  <input type="hidden" id="CarID"  placeholder="รถ" class="form-control" name="carID" value="<?php echo $carID ?>" hidden>

                  <input type="text" id="CusFName" class="form-control" placeholder="ชื่อ" name="FName" value="<?php echo $FName; ?>" readonly readonly>
                </div>
                <?php if (intval($id) == 0) { ?>
                <div class="col-2">
                  <button type="button" class="btn btn-default mb-1" data-toggle="modal" data-target="#mediumModal">
                   เพิ่มข้อมูลรถที่ใช้บริการ
                  </button>
               </div>
              <?php } ?>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">นามสกุล</label>
                <div class="col-10">
                  <input type="text" id="CusLName"  class="form-control" placeholder="นามสกุล" name="LName" value="<?php echo $LName; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">เบอร์โทร</label>
                <div class="col-10">
                  <input type="number" id="CusTel"  class="form-control" placeholder="เบอร์โทร" name="Tel" value="<?php echo $Tel; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">ที่อยู่</label>
                <div class="col-10">
                <input type="text"  id="CusAddress" class="form-control" placeholder="ที่อยู่" name="Address" value="<?php echo $Address; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
              <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1"></label>
                <div class="col-5">
                <input type="text"  id="Cusdistrict" class="form-control" placeholder="ตำบล / แขวง" name="district" value="<?php echo $district; ?>" readonly>
                </div>
                <div class="col-5">
                <input type="text"  id="Cusamphoe" class="form-control" placeholder="อำเภอ" name="amphoe" value="<?php echo $amphoe; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
              <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1"></label>
                <div class="col-5">
                <input type="text"  id="Cusprovince" class="form-control" placeholder="จังหวัด" name="province" value="<?php echo $province; ?>" readonly>
                </div>
                <div class="col-5">
                <input type="text"  id="Cuszipcode" class="form-control" placeholder="รหัสไปรษณีย์" name="zipcode" value="<?php echo $zipcode; ?>" readonly>
                </div>
              </div>
          </div>

          <hr>
            <b>รายละเอียดรถ</b>
          <hr>
          
          
            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ทะเบียนรถ</label>
              <div class="col-8">
              <input type="hidden" class="form-control" placeholder="ไอดีลูกค้าที่เป็นเจ้าของรถปัจจุบัน" id="cusIDs" name="cusIDs" value="<?php echo $cusIDs; ?>" readonly>
                <input type="text" class="form-control" placeholder="ทะเบียนรถ" id="license" name="license" value="<?php echo $license; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">จังหวัด</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="จังหวัด" id="provincelicense" name="province_license" value="<?php echo $province_license; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">วันที่จดทะเบียน</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="วันที่จดทะเบียน" id="registration" name="registration" value="<?php echo $registration; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">รย.</label>
              <div class="col-8">
               <input type="text" class="form-control" placeholder="รย." id="typecar" name="typecar" value="<?php echo $typecar; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ยี่ห้อรถ</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ยี่ห้อรถ" id="brand" name="brand" value="<?php echo $brand; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">รุ่น</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="รุ่น" id="generation" name="generation" value="<?php echo $generation; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขตัวถัง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขตัวถัง" id="bodynumber" name="body_number" value="<?php echo $body_number; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขเครื่อง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขเครื่อง" id="serialnumber" name="serial_number" value="<?php echo $serial_number; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขถังแก๊ส</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขถังแก๊ส" id="gasnumber" name="gas_number" value="<?php echo $gas_number; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ชนิดเชื่อเพลิง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ชนิดเชื่อเพลิง" id="fueltype" name="fuel_type" value="<?php echo $fuel_type; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ขนาดเครื่องยนต์ (ซีซี)</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ขนาดเครื่องยนต์" id="capacity" name="capacity" value="<?php echo $capacity; ?>" readonly>
              </div>
            </div>

            <hr>
              <b>รายละเอียดข้อมูลผู้รับโอน</b>
            <hr>
            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">ชื่อ</label>
              <div class="col-10">
                <input type="text" id="CusLName"  class="form-control" placeholder="" name="RFName" value="<?php echo $RFName; ?>"  >
              </div>
            </div>
            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">นามสกุล</label>
              <div class="col-10">
                <input type="text" id="CusLName"  class="form-control" placeholder="" name="RLName" value="<?php echo $RLName; ?>" >
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">เบอร์โทร</label>
              <div class="col-10">
                <input type="number" id="CusTel"  class="form-control" placeholder="ใส่ตัวเลข 10 ตัวเท่านั้น เช่น 0812345678" name="RTel" value="<?php echo $RTel; ?>" >
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">ที่อยู่</label>
              <div class="col-10">
                <input type="text"  id="CusAddress" class="form-control" placeholder="บ้านเลขที่" name="RAddress" value="<?php echo $RAddress; ?>" >
              </div>
            </div>
            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-1" ></label>
              <div class="col-5">
                <input type="text" class="form-control" placeholder="ตำบล / แขวง" name="Rdistrict" value="<?php echo $Rdistrict; ?>" >
              </div>
              <div class="col-5 ">
                <input type="text" class="form-control" placeholder="อำเภอ / เขต" name="Ramphoe" value="<?php echo $Ramphoe; ?>" >
              </div>
            </div>
            <div class="row form-group">
            <label class="ml-5 mr-10 mt-1 col-sm-1" ></label>
              <div class="col-5 ">
                <input type="text" class="form-control" placeholder="จังหวัด" name="Rprovince" value="<?php echo $Rprovince; ?>" >
              </div>
              <div class="col-5">
                <input type="text" class="form-control" placeholder="รหัสไปรษณีย์" name="Rzipcode" value="<?php echo $Rzipcode; ?>" >
              </div>
            </div>
            <hr>
              <b>รายละเอียดค่าดำเนินการ (ค่าใช้จ่าย)</b>
            <hr>
            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1"></label>
                <div class="col-8"></div>
              <div class="col-2">
                 <button type="button" class="btn btn-default"  onclick="myCreateFunction()">เพิ่มรายการ</button>
              </div>
            </div>


            <div class="row form-group">
            <table class="table table-bordered table-striped"  id="myTable">
              <thead>
              <tr>
                <th>รายการ</th>
                <th>ราคา</th>
                <th>ลบ</th>
              </tr>
              </thead>
              <tbody>
                <?php
                  $index =0;
                    foreach ($tbl as $row) {
                        $index++;
                        $SumTotal = $SumTotal + $row['Price'];
                        ?>
                <tr>
                   <td><input type="text" class="form-control" name="txtName<?php echo $index ?>" value="<?php echo $row['Name'];  ?>"></td>
                   <td><input type="text" class="theClassName txtNumber form-control" name="txtPrice<?php echo $index ?>" value="<?php echo $row['Price'];  ?>" onkeyup="Calculate()"></td>
                   <td align="">
                     <button class="deleteDep btn btn-sm btn-danger text-white" value="Delete" onclick="myFunction(<?php echo $index ?>)">Delete
                   </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>

          <div class="row form-group">
              <label class="ml-5 mr-10 mt-1 col-sm-6" for="exampleInputEmail1"></label>
              <div class="col-2">รวมทั้งหมด : </div>
              <div class="col-1">
                <div id="sum"><?php echo $SumTotal;?></div>
              </div>
              <div class="col-1">
                บาท
              </div>
          </div>

            <div class="form-group">
             <center>
                 <button type="submit" class="btn btn-primary" name="__cmd" value="save">ตกลง</button>
                 <button type="reset" class="btn btn-default" onclick="cancelOnclick()">ยกเลิก</button>
               </center>
           </div>

        <!-- /.card -->
      </div><!-- /.container-fluid -->

      <input type="hidden" id="countTable" name="countTable" value="<?php echo $total;?>" hidden>

      </form>
    </section>
    <!-- /.content -->


  </div>

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>

</div>
<!-- ./wrapper -->

<!-- Modal Customer -->
      <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="mediumModalLabel">ข้อมูลลูกค้า</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">

                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                              <th>ลำดับ</th>
                              <th>ชื่อ-นามสกุล</th>
                              <th>เบอร์โทร</th>
                              <th>ทะเบียนรถ</th>
                              <th>จังหวัด</th>
                              <th>เลือก</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php

                                      $index =0;
                                              foreach ($select_all as $row) {
                                                  $index++;

                                                  ?>
                        <tr>
                          <td><?php echo $index; ?></td>
                          <td><?php echo $row['FName']  ?> <?php echo $row['LName'] ?></td>
                          <td><?php echo $row['tel'] ?></td>
                          <td><?php echo $row['license'] ?></td>
                          <td><?php echo $row['province_license'] ?></td>
                          <td align="center"> <button type="button"  class="btn btn-info" data-dismiss="modal"
                            onclick="addCustomer('<?php echo $row['id']; ?>'
                            ,'<?php echo $row['FName']; ?>'
                            ,'<?php echo $row['LName']; ?>'
                            ,'<?php echo $row['tel']; ?>'
                            ,'<?php echo $row['address']; ?>'
                            ,'<?php echo $row['district']; ?>'
                            ,'<?php echo $row['amphoe']; ?>'
                            ,'<?php echo $row['province']; ?>'
                            ,'<?php echo $row['zipcode']; ?>'
                            ,'<?php echo $row['license']; ?>'
                            ,'<?php echo $row['registration']; ?>'
                            ,'<?php echo $row['typecar']; ?>'
                            ,'<?php echo $row['brand']; ?>'
                            ,'<?php echo $row['generation']; ?>'
                            ,'<?php echo $row['serial_number']; ?>'
                            ,'<?php echo $row['gas_number']; ?>'
                            ,'<?php echo $row['fuel_type']; ?>'
                            ,'<?php echo $row['province_license']; ?>'
                            ,'<?php echo $row['body_number']; ?>'
                            ,'<?php echo $row['cusID']; ?>'
                            ,'<?php echo $row['capacity']; ?>'
                            )" >เลือก</button>
                             </td>
                          </tr>

                            <?php
                                       }
                                   ?>

                        </tbody>
                    </table>

                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                  </div>
              </div>
          </div>
      </div>
      <!-- /.modal -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- datepicker -->
<script src="../../plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js"></script>
<script src="../../plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js"></script>
<!-- address -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>
<script type="text/javascript" src="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dependencies/zip.js/zip.js"></script>
<script type="text/javascript" src="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dependencies/JQL.min.js"></script>
<script type="text/javascript" src="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>
<script type="text/javascript" src="../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>

<script>
  $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>

<script>
  $(document).ready(function () {
      $('.datepicker').datepicker({
          format: 'dd/mm/yyyy',
          todayBtn: true,
          language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
          thaiyear: true              //Set เป็นปี พ.ศ.
      }).datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน

      $('#dataTables-example').DataTable({
          "oLanguage": {
              "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
              "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
              "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
              "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
              "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
              "sSearch": "ค้นหา :",
              "oPaginate": {
                  "sFirst": "เิริ่มต้น",
                  "sPrevious": "ก่อนหน้า",
                  "sNext": "ถัดไป",
                  "sLast": "สุดท้าย"
              }
          },
            responsive: true
        });
  });

  function addCustomer(id,FName,LName,Tel,Address,district,amphoe,province,zipcode,license,registration,
    typecar,brand,generation,serialnumber,gasnumber,fueltype,
    provincelicense,Bodynumber,cusID,capacity) {
        document.getElementById("CusID").value = cusID;
        document.getElementById("CusFName").value = FName;
        document.getElementById("CusLName").value = LName;
        document.getElementById("CusAddress").value = Address;
        document.getElementById("Cusdistrict").value = district;
        document.getElementById("Cusamphoe").value = amphoe;
        document.getElementById("Cusprovince").value = province;
        document.getElementById("Cuszipcode").value = zipcode;
        document.getElementById("CusTel").value = Tel;
        document.getElementById("license").value = license;
        document.getElementById("registration").value = registration;
        document.getElementById("typecar").value = typecar;
        document.getElementById("brand").value = brand;
        document.getElementById("generation").value = generation;
        document.getElementById("serialnumber").value = serialnumber;
        document.getElementById("gasnumber").value = gasnumber;
        document.getElementById("fueltype").value = fueltype;
        document.getElementById("provincelicense").value = provincelicense;
        document.getElementById("bodynumber").value = Bodynumber;
        document.getElementById("CarID").value = id;
        document.getElementById("capacity").value = capacity;

        if (typecar == 'รย.12'){
            document.getElementById("price").value = "324";
        }else if (typecar == 'รย.1'){
              document.getElementById("price").value = "645";
        }else if (typecar == 'รย.2'){
            document.getElementById("price").value = "1282";
        }else if (typecar == 'รย.3'){
            document.getElementById("price").value = "967";
        }
    }

  function cancelOnclick() {
   window.location = 'transferList.php';
  }

  function myCreateFunction() {

       var table = document.getElementById("myTable");
       table.insertRow(table.rows.length).innerHTML =
       '<tr>'
       + '<td><input type="text" class="form-control" name="txtName'+table.rows.length+'" value=""></td>'
       + '<td><input type="text" class="theClassName txtNumber form-control" name="txtPrice'+table.rows.length+'" value="0" onkeyup="Calculate()"></td>'
       + '<td align="" ><button class="deleteDep btn btn-sm btn-danger text-white" value="Delete" onclick="myFunction('+table.rows.length+')">Delete</td>'
       +  '</tr>';

     document.getElementById("countTable").value = table.rows.length;

      Calculate();
   }

   function myFunction(id) {
      $('body').on('click', 'button.deleteDep', function() {
        $(this).parents('tr').remove();
      });

      Calculate();
    }

    function Calculate() {
        var table = document.getElementById('myTable');

        var items = document.getElementsByClassName("theClassName");

        var sum = 0;
        var total = 0;
        for(var i=0; i<items.length; i++){
            var str = "";
            sum += parseInt(items[i].value);
        }

        var output = document.getElementById('sum');
        output.innerHTML = sum;

    }

    $(document).on('keypress', '.txtNumber ', function (event) {
  	    console.log(event.charCode);
  	    event = (event) ? event : window.event;
  	    var charCode = (event.which) ? event.which : event.keyCode;
  	    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
  	        return false;
  	    }
  	    return true;
  	});

      (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-33058582-1', 'auto', {
            'name': 'Main'
        });
        ga('Main.send', 'event', 'jquery.Thailand.js', 'GitHub', 'Visit');


        $.Thailand({
            database: '../../plugins/address/jquery.Thailand.js-master/jquery.Thailand.js/database/db.json', 

            $district: $('#demo1 [name="Rdistrict"]'),
            $amphoe: $('#demo1 [name="Ramphoe"]'),
            $province: $('#demo1 [name="Rprovince"]'),
            $zipcode: $('#demo1 [name="Rzipcode"]'),

            onDataFill: function(data){
                console.info('Data Filled', data);
            },

            onLoad: function(){
                console.info('Autocomplete is ready!');
                $('#loader, .demo').toggle();
            }
        });

        // watch on change

        $('#demo1 [name="district"]').change(function(){
            console.log('ตำบล', this.value);
        });
        $('#demo1 [name="amphoe"]').change(function(){
            console.log('อำเภอ', this.value);
        });
        $('#demo1 [name="province"]').change(function(){
            console.log('จังหวัด', this.value);
        });
        $('#demo1 [name="zipcode"]').change(function(){
            console.log('รหัสไปรษณีย์', this.value);
        });

        function chkString()
{

    if(document.frmMain.RFName.value.length  == "")
    {
    alert('กรุณากรอกชื่อ');
    return false;
    }

    if(document.frmMain.RLName.value.length  == "")
    {
    alert('กรุณากรอกนามสกุล');
    return false;
    }

    if(document.frmMain.RTel.value.length < 10 || document.frmMain.Tel.value.length > 10)
    {
    alert('กรุณาใส่เบอร์โทรศัพท์ให้ถูกต้อง');
    return false;
    }

    if(document.frmMain.RTel.value.length  == "")
    {
    alert('กรุณากรอกเบอร์โทร');
    return false;
    }

    if(document.frmMain.RAddress.value.length  == "")
    {
    alert('กรุณากรอกที่อยู่');
    return false;
    }

    if(document.frmMain.Rdistrict.value.length  == "")
    {
    alert('กรุณากรอกตำบล');
    return false;
    }

    if(document.frmMain.Ramphoe.value.length  == "")
    {
    alert('กรุณากรอกอำเภอ');
    return false;
    }

    if(document.frmMain.Rprovince.value.length  == "")
    {
    alert('กรุณากรอกจังหวัด');
    return false;
    }

    if(document.frmMain.Rzipcode.value.length  == "")
    {
    alert('กรุณากรอกรหัสไปรษณีย์');
    return false;
    }

  }

</script>

</body>
</html>
