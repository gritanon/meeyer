<?php
    session_start();
    require_once "../../ConnectDatabase/connectionDb.inc.php";

    $sql = "SELECT f.carID,f.cusID,f.id,f.startDate,f.endDate,f.refID,f.status,f.methodtype,f.results,f.type,f.type,c.license,c.province_license,cm.FName , cm.LName , cm.Tel
            FROM follow f inner join car c on f.carID = c.id
            inner join customer cm on f.cusID = cm.id 
            where f.methodtype = 'ติดตามแล้ว' ORDER BY f.id DESC";
    $select_all = $conn->queryRaw($sql);
    $total = sizeof($select_all);

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบบริหารจัดการร้าน ตรอ. ช่างใหญ่เซอร์วิส</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php include_once('../includes/sidebar.php') ;
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title d-inline-block">รายการที่ติดตามแล้ว</h3>
          <!-- <a href="actInfo.php" class="btn btn-primary float-right "> + เพิ่มข้อมูลการต่อ พรบ.</a> -->
                 <!-- <a href="actInfo.php" class="btn btn-primary float-right "> + เพิ่มข้อมูลการต่อ พรบ.</a> -->
                        <!-- <a href="actInfo.php" class="btn btn-primary float-right "> + เพิ่มข้อมูลการต่อ พรบ.</a> -->
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive">
          <table id="dataTable" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>ลำดับ</th>
              <th>ประเภทบริการ</th>
              <th>ชื่อ-นามสกุล</th>
              <th>ทะเบียนรถ</th>
              <th>วันที่ครบกำหนด</th>
              <th>ผลการติดตาม</th>
              <th>เปลี่ยนผลการติดตาม</th>
            </tr>
            </thead>
            <tbody>
              <?php
                $index =0;
                  foreach ($select_all as $row) {
                      $index++;
                      ?>
              <tr>
                <td><?php echo $index; ?></td>
                <td><?php echo $row['type']  ?><?php echo $row['carID']  ?><?php echo $row['cusID']  ?></td>
                <td><?php echo $row['FName']  ?> <?php echo $row['LName'] ?></td>
                <td><?php echo $row['license'] ?> <?php echo $row['province_license'] ?></td>
                <td><?php echo convertDateThai($row['endDate'])  ?></td>
                <?php if ($row['results'] == '') { ?>
                          <td class=" text-primary font-weight-bold">รอการดำเนินการ</td>
                <?php } else { ?>
                          <td  class=" text-primary font-weight-bold"><?php echo $row['results'] ?></td>
                <?php } ?>
                <td align="center">
                  <a class="btn btn-sm btn-warning text-white" href="updatefollowback.php?id=<?php echo $row['id'];?>&type=<?php echo $row['type']; ?>&carID=<?php echo $row['carID']; ?>&cusID=<?php echo $row['cusID']; ?>">กลับมา</a>
                  <a class="btn btn-sm btn-danger text-white" onclick="EditFollow2Onclick(<?php echo $row['id']; ?>)">เลิกติดตาม</a>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>

</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>


<script>
  $(function () {
    $('#dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                 "oPaginate": {
                     "sFirst": "เิริ่มต้น",
                     "sPrevious": "ก่อนหน้า",
                     "sNext": "ถัดไป",
                     "sLast": "สุดท้าย"
                 }
             }
    });
  });

  // function EditFollowOnclick(id,type) {
  //   if(confirm('คุณต้องการเปลี่ยนผลการติดตาม ใช่ หรือ ไม่?') == true)
  //      window.location = 'updatefollowback.php?id=' + id + '&type=' + type;
  // }

  function EditFollow2Onclick(id) {
    if(confirm('คุณต้องการเปลี่ยนผลการติดตาม ใช่ หรือ ไม่?') == true)
       window.location = 'updatefollownotback.php?id=' + id  ;
  }

  function reportPrint(id) {
    window.open("letter-print.php?id=" + id);
  }

  function DetailOnclick(id) {
      window.location = 'actDetail.php?id=' + id;
  }

</script>

</body>
</html>
