<?php

session_start();
require_once "../../ConnectDatabase/connectionDb.inc.php";

$id = getIsset("id");
$cmd = getIsset("__cmd");

$persent = 0;
$amount = 1;
$month = 0;

if (intval($id) > 0){

  $tbl_Follow = $conn->select('follow', array('id' => getIsset('id')), true);

  if($tbl_Follow != null){
    $status = $tbl_Follow["status"];
    $methodtype = $tbl_Follow["methodtype"];

    $tbl_tax = $conn->select('tax', array('id' => $tbl_Follow["refID"]), true);

      $cusID = $tbl_tax["cusID"];
      $carID = $tbl_tax["carID"];
      $Date = $tbl_tax["Date"];
      $taxYear = $tbl_tax["taxYear"];
      $persent = $tbl_tax["persent"];
      $discount = $tbl_tax["discount"];
      $amount = $tbl_tax["amount"];
      $subTotal = $tbl_tax["subTotal"];
      $month = $tbl_tax["month"];
      $fine = $tbl_tax["fine"];
      $total = $tbl_tax["total"];
      $startDate = convertDateThai($tbl_tax["startDate"]);
      $endDate = convertDateThai($tbl_tax["endDate"]);

      $tbl_car = $conn->select('car', array('id' => $carID), true);
      $license = $tbl_car["license"];
      $province_license = $tbl_car["province_license"];
      $registration = convertDateThai($tbl_car["registration"]);
      $typecar = $tbl_car["typecar"];
      $brand = $tbl_car["brand"];
      $generation = $tbl_car["generation"];
      $body_number = $tbl_car["body_number"];
      $serial_number = $tbl_car["serial_number"];
      $gas_number = $tbl_car["gas_number"];
      $fuel_type = $tbl_car["fuel_type"];
      $capacity = $tbl_car["capacity"];

      $tbl_cus = $conn->select('customer', array('id' => $cusID), true);
      $FName = $tbl_cus["FName"];
      $LName = $tbl_cus["LName"];
      $Address = $tbl_cus["Address"];
      $Tel = $tbl_cus["Tel"];
    }
  }

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบบริหารจัดการร้าน ตรอ. ช่างใหญ่เซอร์วิส</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../../plugins/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php include_once('../includes/sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>ข้อมูลการต่อภาษี</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <li class="breadcrumb-item"><a href="../customer">ข้อมูลการต่อภาษี</a></li> -->
              <li class="breadcrumb-item active">ข้อมูลการต่อภาษี</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
    <form>
      <div class="container-fluid">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">ข้อมูลการต่อภาษี</h3>
          </div>
          <div class="col-12 table-responsive card-body">
            <b class="">รายละเอียดข้อมูลเจ้าของรถ</b>
            <hr>
            <div class="card-body">
              <input type="hidden" name="id" value="<?php echo $id ?>">
              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">ชื่อ</label>
              <?php if (intval($id) == 0) { ?>
                <div class="col-8">
                <?php } else { ?>
                <div class="col-10">
                <?php } ?>
                  <input type="hidden" id="CusID"  placeholder="ชื่อ" class="form-control" name="cusID" value="<?php echo $cusID ?>" hidden>
                  <input type="hidden" id="CarID"  placeholder="รถ" class="form-control" name="carID" value="<?php echo $carID ?>" hidden>

                  <input type="text" id="CusFName" class="form-control" placeholder="ชื่อ" name="FName" value="<?php echo $FName; ?>" readonly readonly>
                </div>
                <?php if (intval($id) == 0) { ?>
                <div class="col-2">
                  <button type="button" class="btn btn-default mb-1" data-toggle="modal" data-target="#mediumModal">
                   เพิ่มข้อมูลลูกค้า
                  </button>
               </div>
              <?php } ?>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">นามสกุล</label>
                <div class="col-10">
                  <input type="text" id="CusLName"  class="form-control" placeholder="นามสกุล" name="LName" value="<?php echo $LName; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">เบอร์โทร</label>
                <div class="col-10">
                  <input type="number" id="CusTel"  class="form-control" placeholder="เบอร์โทร" name="Tel" value="<?php echo $Tel; ?>" readonly>
                </div>
              </div>

              <div class="row form-group">
                  <label class="ml-5 mr-10 mt-1 col-sm-1" for="exampleInputEmail1">ที่อยู่</label>
                <div class="col-10">
                  <input type="text"  id="CusAddress" class="form-control" placeholder="ที่อยู่" name="Address" value="<?php echo $Address; ?>" readonly>
                </div>
              </div>

          </div>

          <hr>
            <b>รายละเอียดรถ</b>
          <hr>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ทะเบียนรถ</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ทะเบียนรถ" id="license" name="license" value="<?php echo $license; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">จังหวัด</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="จังหวัด" id="provincelicense" name="province_license" value="<?php echo $province_license; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">วันที่จดทะเบียน</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="วันที่จดทะเบียน" id="registration" name="registration" value="<?php echo $registration; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">รย.</label>
              <div class="col-8">
               <input type="text" class="form-control" placeholder="รย." id="typecar" name="typecar" value="<?php echo $typecar; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ยี่ห้อรถ</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ยี่ห้อรถ" id="brand" name="brand" value="<?php echo $brand; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">รุ่น</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="รุ่น" id="generation" name="generation" value="<?php echo $generation; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขตัวถัง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขตัวถัง" id="bodynumber" name="body_number" value="<?php echo $body_number; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขเครื่อง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขเครื่อง" id="serialnumber" name="serial_number" value="<?php echo $serial_number; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">หมายเลขถังแก๊ส</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="หมายเลขถังแก๊ส" id="gasnumber" name="gas_number" value="<?php echo $gas_number; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ชนิดเชื่อเพลิง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ชนิดเชื่อเพลิง" id="fueltype" name="fuel_type" value="<?php echo $fuel_type; ?>" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ขนาดเครื่องยนต์ (ซีซี)</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="ขนาดเครื่องยนต์" id="capacity" name="capacity" value="<?php echo $capacity; ?>" readonly>
              </div>
            </div>

            <hr>
              <b>ข้อมูลการต่อภาษี</b>
            <hr>
            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">วันเริ่มต้นการคุ้มครอง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="วันเริ่มต้นคุ้มครอง" id="startDate" name="startDate" value="<?php echo $startDate; ?>" onchange="selectStartDate()" readonly>
              </div>
            </div>

            <div class="row form-group">
                <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">วันสิ้นสุดการคุ้มครอง</label>
              <div class="col-8">
                <input type="text" class="form-control" placeholder="วันสิ้นสุดการคุ้มครอง" id="endDate" name="endDate" value="<?php echo $endDate; ?>" readonly>
              </div>
            </div>
              <div class="row form-group">
                    <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ค่าภาษีปีละ (บาท)</label>
                  <div class="col-8">
                    <input type="text" class="form-control" placeholder="ค่าภาษี (ปี)" name="taxYear" id="taxYear" value="<?php echo $taxYear; ?>" readonly>
                  </div>
              </div>
              <div class="row form-group">
                    <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ส่วนลด</label>
                  <div class="col-1">
                    <input type="text" class="form-control txtNumber" placeholder="%" name="persent" maxlength="3" id="persent" value="<?php echo $persent; ?>"readonly>
                  </div>
                  <label class="col-1 mt-1" for="exampleInputEmail1">%</label>
                  <div class="col-6">
                    <input type="text" class="form-control" placeholder="ส่วนลด" name="discount" id="discount" value="<?php echo $discount; ?>" readonly>
                  </div>
              </div>
              <div class="row form-group">
                    <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ต่อภาษีจำนวน (ปี)</label>
                  <div class="col-8">
                    <input type="text" class="form-control txtNumber" placeholder="ต่อภาษีจำนวน (ปี)" maxlength="2" name="amount" id="amount" value="<?php echo $amount; ?>"readonly>
                  </div>
              </div>
              <div class="row form-group">
                    <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">เป็นเงิน</label>
                  <div class="col-8">
                    <input type="text" class="form-control" placeholder="เป็นเงิน" name="subTotal" id="subTotal" value="<?php echo $subTotal; ?>" readonly>
                  </div>
              </div>
              <div class="row form-group">
                    <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">จำนวนเดือนปรับ</label>
                  <div class="col-8">
                    <input type="text" class="form-control txtNumber" placeholder="จำนวนเดือนปรับ" maxlength="2" name="month" id="month" value="<?php echo $month; ?>"readonly>
                  </div>
              </div>
              <div class="row form-group">
                    <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">ค่าปรับ</label>
                  <div class="col-8">
                    <input type="text" class="form-control" placeholder="ค่าปรับ" name="fine" id="fine" value="<?php echo $fine; ?>" readonly>
                  </div>
              </div>
              <div class="row form-group">
                    <label class="ml-5 mr-10 mt-1 col-sm-2" for="exampleInputEmail1">รวมเป็น</label>
                  <div class="col-8">
                    <input type="text" class="form-control" placeholder="รวมเป็น" name="total" id="total" value="<?php echo $total; ?>" readonly>
                  </div>
              </div>




            <div class="form-group">
             <center>
                 <button type="reset" class="btn btn-default" onclick="cancelOnclick()">ปิด</button>
               </center>
           </div>

        <!-- /.card -->
      </div><!-- /.container-fluid -->
      </form>
    </section>
    <!-- /.content -->
  </div>

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>

</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="../../plugins/datepicker/locales/bootstrap-datepicker.th.js"></script>
<script src="../../plugins/datepicker/bootstrap-datepicker-thai.js"></script>
<!-- datepicker -->
<script src="../../plugins/bootstrap-datepicker/js/bootstrap-datepicker-custom.js"></script>
<script src="../../plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js"></script>

<script>
  function cancelOnclick() {
   window.location = 'tax.php';
  }

</script>

</body>
</html>
