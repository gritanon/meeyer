<?php 
  session_start();
  require_once "../../ConnectDatabase/connectionDb.inc.php";

  $sql = "SELECT UserID,Tel,Name,Address,Username,status
  FROM data_user WHERE status='employee' or status='manager' ORDER BY UserID DESC";
  $select_all = $conn->queryRaw($sql);
  $total = sizeof($select_all);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>User Management</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="../../dist/img/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="../../dist/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="../../dist/img/favicons/favicon-16x16.png">
  <link rel="manifest" href="../../dist/img/favicons/site.webmanifest">
  <link rel="mask-icon" href="../../dist/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="../../dist/img/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="../../dist/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar & Main Sidebar Container -->
  <?php include_once('../includes/check_sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>User Management</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="../dashboard">Dashboard</a></li>
              <li class="breadcrumb-item active">User Management</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title d-inline-block">User List</h3>
          <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#myModal">USER +</button>
          <!-- Modal -->
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg">
            <form  name="frmMain" OnSubmit="return chkString();"  action="create.php" method="post" enctype="multipart/form-data">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">เพิ่มผู้ใช้</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                  
<!-- general form elements -->
                    <div class="col-md-12">                   
                      <div class="form-group">
                        <label>ชื่อ-นามสกุล</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="text" class="form-control" placeholder="ชื่อ-นามสกุล"  name="name">
                          </div>
                        </div>
                        <label class="mt-2">ชื่อผู้ใช้</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="text" class="form-control" placeholder="ใส่มากกว่า 8 ตัวอักษร"  name="username">
                          </div>
                        </div>
                        <label  class="mt-2">รหัสผ่าน</label>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="ใส่มากกว่า 8 ตัวอักษร"  name="password">
                        </div>
                        <label  class="mt-2">ทีอยู่</label>
                        <div class="input-group">
                          <textarea class="form-control" rows="2" id="comment" name="address"></textarea>
                        </div>
                        <label  class="mt-2">เบอร์โทร</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="tel">
                        </div>
                        <label class="mt-2">สถานะ</label>
                        <div class="input-group">
                          <div class="form-group">
                            <select class="form-control" name="Status">
                              <option>กรุณาเลือกสถานะ</option>
                              <option>employee</option>
                              <option>manager</option>
                            </select>
                          </div>
                        </div>
                     </div>
                    </div>

                  </div>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary" >เพิ่ม</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
              </form>
            </div>
          </div>
      </div>
          
        <!-- /.card-header -->
        <div class="card-body table-responsive">
          <table id="dataTable" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No.</th>
              <th>ชื่อผู้ใช้</th>
              <th>ชื่อ</th>
              <th>ที่อยู่</th>
              <th>เบอร์โทร</th>
              <th>สถานะ</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php
                $index =0;
                  foreach ($select_all as $row) {
                      $index++;
              ?>
              <tr>
                <td><?php echo $index; ?></td>
                <td>                  
                    <?php echo $row["Username"];?>
                </td>
                <td><?php echo $row['Name']  ?></td>
                <td><?php echo $row['Address']  ?></td>
                <td><?php echo $row['Tel']  ?></td>
                <td><?php echo $row['status']  ?></td>
                <td>
                  <a href="form_edit.php?UserID=<?php echo $row['UserID'] ?>" class="btn btn-sm btn-warning text-white">
                    <i class="fas fa-edit"></i> edit
                  </a> 
                </td>
                <td>
                  <a href="#" onclick="deleteItem(<?php echo $row['UserID'] ?>);"  class="btn btn-sm btn-danger">
                    <i class="fas fa-trash-alt"></i> Delete
                  </a>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- footer -->
  <?php include_once('../includes/footer.php') ?>
  
</div>

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap4.min.js"></script>

<script>
    $(function () {
      $('#dataTable').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
      });
    });

    function deleteItem (UserID) { 
    if( confirm('แน่ใจหรือไม่ที่จะลบสิ่งนี้') == true){
      window.location='delete.php?UserID='+UserID;
    }
    };


    function switchedit() {
        document.getElementById("myDIVa1").style.display = "none";
        document.getElementById("myDIVb1").style.display = "";
        document.getElementById("myDIVa2").style.display = "none";
        document.getElementById("myDIVb2").style.display = "";
    }


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function chkString(){

      if(document.frmMain.name.value.length  == "")
      {
        alert('กรุณากรอกชื่อ-นามสกุล');
        return false;
      }

      if(document.frmMain.username.value.length < 8)
      {
        alert('กรุณาใส่ชื่อผู้ใช้ให้มากกว่า 8 ตัวอักษร');
        return false;
      }

      if(document.frmMain.password.value.length < 8)
      {
        alert('กรุณาใส่รหัสผ่านให้มากกว่า 8 ตัวอักษร');
        return false;
      }

      if(document.frmMain.Status.value.length ='')
      {
        alert('กรุณาระบุสถานะผู้ใช้');
        return false;
      }
    }

</script>

</body>
</html>
