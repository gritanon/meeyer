<?php
        require_once "../../ConnectDatabase/connectionDb.inc.php";

?>

<?php
    $link = $_SERVER['REQUEST_URI'];
    $link_array = explode('/',$link);
    $name = $link_array[count($link_array) - 2];
?>
<nav class="main-header navbar navbar-expand border-bottom navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-3">
      <li class="d-none d-sm-block"><b>ระบบบริหารจัดการ สถานตรวจสภาพรถเอกชน</b></li>
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
        <img src="../../dist/img/logochangyai-128x128.png" alt="" class="img-size-32 mr-3 img-circle">
          <span class="badge navbar-badge"  style="background-color:#1ae849;color:white;font-weight:bolder;"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="../../dist/img/logochangyai-128x128.png" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  ตรอ. ช่างใหญ่เซอร์วิส
                  <span class="float-right text-sm text-primary"><i class="fa fa-star"></i></span>
                </h3>
                <p class="text-sm"><?php echo $_SESSION["Name"] ?></p>
                <p class="text-sm text-muted"></i> ผู้ดูแลระบบ </p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider" ></div>
          <a href="../../logout.php" class="dropdown-item dropdown-footer">
            <i class="fas fa-sign-out-alt"></i>
            ออกจากระบบ
          </a>
        </div>
      </li>
    </ul>
</nav>
  <!-- /.navbar -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../dashboard/" class="brand-link">
      <img  src="../../dist/img/logochangyai-128x128.png" 
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">ช่างใหญ่เซอร์วิส</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel d-flex">
        <div class="image">
        
        </div>
        <div class="info">
          <a href="../dashboard/" class="d-block">สถานตรวจสภาพรถเอกชน</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2" >
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="../admin" class="nav-link <?php echo $name == 'admin' ? 'active': '' ?>">
              <i class="fas fa-users-cog nav-icon"></i>
              <p>ข้อมูลผู้ใช้</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../promotion" class="nav-link <?php echo $name == 'promotion' ? 'active': '' ?>">
              <i class="fas fa-gift  nav-icon"></i>
              <p>โปรโมชั่น</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../news" class="nav-link <?php echo $name == 'news' ? 'active': '' ?>">
              <i class="far fa-newspaper nav-icon"></i>
              <p>ข่าว</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../file/" class="nav-link <?php echo $name == 'file' ? 'active': '' ?>">
              <i class="fas fa-file-alt nav-icon"></i>
              <p>ไฟล์เอกสาร</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../survey/" class="nav-link <?php echo $name == 'survey' ? 'active': '' ?>">
            <i class="fas fa-tasks nav-icon"></i>
              <p>แบบสอบถาม</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../contacts/" class="nav-link <?php echo $name == 'contacts' ? 'active': '' ?>">
              <i class="fas fa-chalkboard-teacher nav-icon"></i>
              <p>ข้อมูลการติดต่อ</p>
            </a>
          </li>
          <li class="nav-header">Account Settings</li>
          <li class="nav-item">
            <a href="../admin/form_edit.php?UserID=<?php echo $_SESSION["UserID"]; ?>" class="nav-link <?php echo $name == 'profile' ? 'active': '' ?>">
              <i class="fas fa-user-edit"></i>
              <p class="ml-1">Profile</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="../../logout.php" class="nav-link">
              <i class="fas fa-sign-out-alt"></i>
              <p class="ml-2">Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
