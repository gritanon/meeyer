<?php
    //กำหนดสิทธการใช้งาน

    switch ($_SESSION["status"]) {
        case "employee":
            include_once('../includes/sidebar.php');
            break;
        case "admin":
            include_once('../includes/sidebar_admin.php');
            break;
        case "manager":
            include_once('../includes/sidebar_manager.php');
            break;
        default:
            header('Location: ../../login.php');
    }
?>
